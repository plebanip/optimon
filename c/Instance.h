#ifndef __INSTANCE__
#define __INSTANCE__
#include <sstream>
#include <string>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <cmath>
#include <deque>
#include <iomanip>
//#include "gurobi_c++.h"



using namespace std;

struct SolutionData {
	int** w; // M x Microprocesso
	int** x; // Microprocesso x Sito
	int*** z;
	int*** y;
	int** u;
	int** vv;
	double l;
};

struct Punto {
	double x;
	double y;
};

class Instance{
private:
	bool hasSolution;

	// dimensioni generali
	int NDimensioni;
	int NMetriche;
	int NMetricMeasurements;
	int NSiti;
	int NMicroProcessi;


	// parametri costruzione istanza
	int nIstanzeMicroprocessi;
	int nCoppieAttivitaSTESSOSito;
	int nCoppieAttivitaDIVERSOSito;
	int nMetricheDaMisurare;
	int nMicropocessiInSiti;
	
	double probPrecisioneZero;

	double costoMaxVal;
	double costoMinVal;
	double costoMaxSonda;
	double costoMinSonda;
	double costoMax;
	double costoMin;
	
	double probOverlapM;
	double probOverlapMM;

	// dati istanza
	int** DimensionRequests; // NMicroProcessi x NDimensioni 
	int** MetricheDaMisurare; // nMetricheDaMisurare x 2 (metrica, microprocesso)

	int** ElencoAttivitaDIVERSO; // nCoppieAttivitaSTESSOSito x 2 (microprocesso1, microprocesso2) 
	int** ElencoAttivitaSTESSO; // nCoppieAttivitaDIVERSOSito x 2 (microprocesso1, microprocesso2)

	vector<int> * MicropocessiInSiti;

	double budget;
	double minPrecisione;
	double maxPrecisione;

	double** probNInstancePMF;//Nmicroprocessi x nIstanzeMicroprocessi

	double*** Precisione; //MetricMeasurement x Siti
	double*** Delta1Precisione; //MetricMeasurement x Siti
	double*** Delta2Precisione; //MetricMeasurement x Siti

	double* CostoUnitarioSito; // Siti

	double** CostoValutazione; //MetricMeasurement x Siti
	double** CostoSonda; //MetricMeasurement x Siti

	vector<int>*  MforDimension; // dimension x Metric
	vector<int>*  MMforMetric; // Metric x mm
	
	vector<Punto> paretoFront;
	// pesi della funzioni obiettivo

	double* lambdas;

	// soluzione al problema
	double objFunction;

	SolutionData sol;
	
	// varie
	int* distributionType;
	int* numberInstance;

	int***y_Recorded;

	int sumZ_OLD;
	int sumY_OLD;
	double accuracy_OLD;

//	GRBEnv env ;
//	GRBModel  model;

public:
	
	
	Instance(int NDimensioni, int NMetriche, int NMetricMeasurements, int NMicroProcessi, int NSiti, double probPrecisioneZero,
		int nIstanzeMicroprocessi = 50, int nCoppieAttivitaSTESSOSito = 0, int nCoppieAttivitaDIVERSOSito = 0, 
		int nMicropocessiInSiti = 0,int nMetricheDaMisurare = 0, double budget = -1, double costoMax = 100.0, double costoMin = 50.0,
		double costoMaxVal = 1.0, double costoMinVal = 0.5, double costoMaxSonda = 10.0,	double costoMinSonda = 5.0,	
		double probOverlapM = 0.1, double probOverlapMM = 0.0, double maxPrecisione = 0.6, double minPrecisione = 0.1, double probNormalInstanceNumber = 0.3);

	Instance(string path);

	virtual ~Instance() {};


	void initialize();

	void solveParetoFront(int maxPointsFree = -1, int setPoint = 100, bool saveSol = true, bool plotPoints = true, bool verbose = false);
	void dumpSolution(string path);
	

	void generate(bool verbose = false);
	
	void printInstance(string path, int index = -1);
	


	void setGradimentoMetriche(vector<double> Gradimento);
	void generateKBTree(bool verbose = false);
	void dumpKBTree(string path);
	
	void dumpInstance(string path); 
	void dumpMicroProcesso(string path, int nMicroProcesso=0);
	void dumpSito(string path, int nSito = 0);
	void setBudget(double budget);

	

	double solveTest(int nScenarios, string path, int i = -1,int extracost=-1, bool verbose = false);
	

	void solveParetoFront_MIN(int maxPointsFree = -1, int setPoint = 100, bool saveSol = true, double GAP=-1, int timeLimit=-1, bool plotPoints = true, bool verbose = false, bool verboseModel = false);
	void dumpSolution_MIN(string path);
	void dumpSolutionEssential_MIN(string path, int index);
	
	void readSolutionEssential_MIN(string path, bool verbose = false);


	int getY(int mm, int s, int v);

	void plotPareto(string path, int index = -1);

	// void plotParetoLatex(string path); // accoda al file che apre.

	int nEstimatedMM();
	int nMadeMM();
	double solutionCost();
	double solutionAccuracy();

	
	void testSolutionInStochastic(int nScenarios, string path, int i=-1); 
	int realizationMicroprocess(int nMP);

	void modifyMeanwithQuantile(double quantile);
	int quantileMicroprocess(double quantile, int nMP);

	void printSiteSolution(int nSito, string path);
	
	
	
};

#endif

