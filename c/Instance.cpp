#include <iostream>
#include <random>
#include <stdio.h>
#include <direct.h>

#include "gurobi_c++.h"
#include "instance.h"


using namespace std;

Instance::Instance(int NDimensioni, int NMetriche, int NMetricMeasurements, int NMicroProcessi, int NSiti, double probPrecisioneZero,
	int nIstanzeMicroprocessi, int nCoppieAttivitaSTESSOSito, int nCoppieAttivitaDIVERSOSito,
	int nMicropocessiInSiti, int nMetricheDaMisurare, double budget, double costoMax, double costoMin,
	double costoMaxVal, double costoMinVal, double costoMaxSonda, double costoMinSonda,
	double probOverlapM, double probOverlapMM, double maxPrecisione, double minPrecisione, double probNormalInstanceNumber){

	this->probOverlapM = probOverlapM;
	this->probOverlapMM = probOverlapMM;

	this->costoMax = costoMax;
	this->costoMin = costoMin;

	this->costoMaxVal = costoMaxVal;
	this->costoMinVal = costoMinVal;

	this->costoMaxSonda = costoMaxSonda;
	this->costoMinSonda = costoMinSonda;

	this->NDimensioni = NDimensioni;
	this->NMetriche = NMetriche;
	this->NMicroProcessi = NMicroProcessi;
	this->NMetricMeasurements = NMetricMeasurements;
	this->NSiti = NSiti;
	this->probPrecisioneZero = probPrecisioneZero;
	if (NMicroProcessi < 10) {
		cout << "numero di microprocessi troppo basso";
		cin.get();
		exit(1);
	}
	if (NMicroProcessi >= 10 && NSiti >= 10 && NMetricMeasurements >= 15) {
		if (nCoppieAttivitaSTESSOSito == 0) this->nCoppieAttivitaSTESSOSito = max(0.1*NMicroProcessi, 1.0);
		if (nCoppieAttivitaDIVERSOSito == 0) this->nCoppieAttivitaDIVERSOSito = max(0.1*NMicroProcessi, 1.0);
		if (nMetricheDaMisurare == 0) this->nMetricheDaMisurare = max(0.05*NMetriche, 1.0);
		if (nMicropocessiInSiti == 0) this->nMicropocessiInSiti = max(0.05*NMetriche, 1.0);
	}

	lambdas = new double[NDimensioni];
	double sum = 0;
	for (int i = 0; i < NDimensioni; i++) {
		lambdas[i] = (double)rand() / (RAND_MAX);
		sum += lambdas[i];
	}

	for (int i = 0; i < NDimensioni; i++) lambdas[i] = lambdas[i] / sum;
	
	this->minPrecisione = minPrecisione;
	this->maxPrecisione = maxPrecisione;
	hasSolution = false;

	// stocastic nature
	distributionType = new int[NMicroProcessi];
	numberInstance = new int[NMicroProcessi];

	this->nIstanzeMicroprocessi = nIstanzeMicroprocessi;

	probNInstancePMF = new double*[NMicroProcessi];
	for (int i = 0; i < NMicroProcessi; i++) {
		numberInstance[i] = 0.0; // inizializzazione della media punti
		probNInstancePMF[i] = new double[nIstanzeMicroprocessi];
		double sum = 0.0;
		double media = 0.0;
		double varianza = 0.0;
		bool normal = false;
		bool exponential = false;

		if ((double)rand() / (RAND_MAX) < probNormalInstanceNumber) {
			normal = true;
			exponential = false;
			media = rand() % nIstanzeMicroprocessi;
			varianza = max(rand() % nIstanzeMicroprocessi / 2,1);
			distributionType[i] = 0;
			//cout<<i<<" "<< media << " " << varianza << endl;
		}
		else {
			normal = false;
			exponential = true;
			distributionType[i] = 1;
		}

		for (int j = 0; j < nIstanzeMicroprocessi; j++) {
			if (exponential)	probNInstancePMF[i][j] = exp(-2.5*((double)j) / ((double)nIstanzeMicroprocessi)) *(double)rand() / (RAND_MAX);
			if (normal) probNInstancePMF[i][j] = exp(-2.5*pow(((double)j) - media, 2.0) / (2.0*pow(varianza, 2.0))); // *(double)rand() / (RAND_MAX);
			sum += probNInstancePMF[i][j];
		}
		for (int j = 0; j < nIstanzeMicroprocessi; j++) {
			probNInstancePMF[i][j] = probNInstancePMF[i][j] / sum;
		}

	}


	for (int i = 0; i < NMicroProcessi; i++) {
		double sum = 0.0;;
		for (int j = 0; j < nIstanzeMicroprocessi; j++) {
			sum += ((double)j)*probNInstancePMF[i][j];
		}
		numberInstance[i] =  ceil(sum);
		// if (numberInstance[i] == 0) cout << "PROBLEMA" << endl;
	}

	if (budget == -1) {
		int nInstance = 0;
		for (int i = 0; i < NMicroProcessi; i++) {
			nInstance += numberInstance[i];
		}
		//nInstance = 1;
		this->budget = 10*costoMax*nInstance*NMicroProcessi;
	}
	else {
		this->budget = budget;
	}
	/******************************************************************/
	sol.x = new int*[NMicroProcessi];
	for (int v = 0; v < NMicroProcessi; v++) {
		sol.x[v] = new int[NSiti];
		for (int s = 0; s < NSiti; s++) {
			sol.x[v][s] = 0;
		}
	}
	sol.y = new int**[NMetricMeasurements];
	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		sol.y[mm] = new int*[NSiti];
		for (int s = 0; s < NSiti; s++) {
			sol.y[mm][s] = new int[NMicroProcessi];
			for (int v = 0; v < NMicroProcessi; v++) {
				sol.y[mm][s][v] = 0;
			}
		}
	}
	sol.z = new int**[NMetricMeasurements];
	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		sol.z[mm] = new int*[NSiti];
		for (int s = 0; s < NSiti; s++) {
			sol.z[mm][s] = new int[NMicroProcessi];
			for (int v = 0; v < NMicroProcessi; v++) {
				sol.z[mm][s][v] = 0;
			}
		}
	}
	sol.w = new int*[NMetriche];
	for (int m = 0; m < NMetriche; m++) {
		sol.w[m] = new int[NMicroProcessi];
		for (int v = 0; v < NMicroProcessi; v++) {
			sol.w[m][v] = 0;
		}
	}
	sol.vv = new int*[NMetriche];
	for (int m = 0; m < NMetriche; m++) {
		sol.vv[m] = new int[NMicroProcessi];
		for (int s = 0; s < NMicroProcessi; s++) {
			sol.vv[m][s] = 0;
		}
	}
	sol.u = new int*[NDimensioni];
	for (int d = 0; d < NDimensioni; d++) {
		sol.u[d] = new int[NMicroProcessi];
		for (int s = 0; s < NMicroProcessi; s++) {
			sol.u[d][s] = 0;
		}
	}

	sumZ_OLD = -1;
	sumY_OLD = -1;
	accuracy_OLD = -1.0;
}

Instance::Instance(string path){
	string line;
	string word;

	ifstream iffN(path.c_str());

	if (!iffN.is_open()) {
		cout << "Impossible to open" << path << endl;
		cin.get();
		exit(1);
	}

	sumZ_OLD = -1;
	sumY_OLD = -1;
	accuracy_OLD = -1.0;

	std::getline(iffN, line); // scarto la prima riga, nella prima riga (fileO << "General Coeff" << endl;)
	std::getline(iffN, line);
	istringstream iss_1(line);
	iss_1 >> word; this->NDimensioni = atoi(word.c_str());
	iss_1 >> word; this->NMetriche = atoi(word.c_str());
	iss_1 >> word; this->NMetricMeasurements = atoi(word.c_str());
	iss_1 >> word; this->NMicroProcessi = atoi(word.c_str());
	iss_1 >> word; this->NSiti = atoi(word.c_str());
	std::getline(iffN, line);
	istringstream iss_2(line);
	iss_2 >> word; this->nCoppieAttivitaSTESSOSito = atoi(word.c_str());
	iss_2 >> word; this->nCoppieAttivitaDIVERSOSito = atoi(word.c_str());
	iss_2 >> word; this->nMicropocessiInSiti = atoi(word.c_str());
	iss_2 >> word; this->nMetricheDaMisurare = atoi(word.c_str());
	std::getline(iffN, line);
	istringstream iss_3(line);
	//iss_3 >> word; this->nIstanzeMicroprocessi = atoi(word.c_str());
	iss_3 >> word; this->budget = atof(word.c_str());
	iss_3 >> word; this->minPrecisione = atof(word.c_str());
	std::getline(iffN, line); // fileO << endl;
	this->initialize();

	// PRECISIONI
	//std
	std::getline(iffN, line); // fileO << "Std Prec" << endl;
	for (int i = 0; i < nIstanzeMicroprocessi; i++) {
		std::getline(iffN, line); // fileO << endl;
		for (int m = 0; m < NMetricMeasurements; m++) {
			std::getline(iffN, line); istringstream iss(line);
			for (int s = 0; s < NSiti; s++) {
				iss >> word; Precisione[m][s][i] = atof(word.c_str());
			}
		}
	}
	std::getline(iffN, line); //fileO << endl;

						 //stima
	std::getline(iffN, line); //fileO << "Stim Prec" << endl;
	for (int i = 0; i < nIstanzeMicroprocessi; i++) {
		std::getline(iffN, line); // fileO << endl;
		for (int m = 0; m < NMetricMeasurements; m++) {
			std::getline(iffN, line); istringstream iss(line);
			for (int s = 0; s < NSiti; s++) {
				iss >> word; Delta1Precisione[m][s][i] = atof(word.c_str());
			}
		}
	}
	std::getline(iffN, line); // fileO << endl;

						 //make	
	std::getline(iffN, line); // fileO << "Stim Meas" << endl;
	for (int i = 0; i < nIstanzeMicroprocessi; i++) {
		std::getline(iffN, line); // fileO << endl;
		for (int m = 0; m < NMetricMeasurements; m++) {
			std::getline(iffN, line); istringstream iss(line);
			for (int s = 0; s < NSiti; s++) {
				iss >> word; Delta2Precisione[m][s][i] = atof(word.c_str());
			}
		}
	}
	std::getline(iffN, line);

	// MICROPROCESSI CHE DEVONO STARE INSIEME OPPURE STACCATI o IN SITI
	std::getline(iffN, line); //fileO << "Constr Same Site" << endl;
	for (int i = 0; i < nCoppieAttivitaSTESSOSito; i++) {
		std::getline(iffN, line); istringstream iss(line);
		for (int j = 0; j < 2; j++) {
			iss >> word;  ElencoAttivitaSTESSO[i][j] = atoi(word.c_str());
		}
	}
	std::getline(iffN, line); //fileO << endl;

	std::getline(iffN, line); //fileO << "Constr Different Site" << endl;

	for (int i = 0; i < nCoppieAttivitaDIVERSOSito; i++) {
		std::getline(iffN, line); istringstream iss(line);
		for (int j = 0; j < 2; j++) {
			iss >> word;  ElencoAttivitaDIVERSO[i][j] = atoi(word.c_str());
		}
	}
	std::getline(iffN, line); //fileO << endl;


	std::getline(iffN, line); //fileO << "Constr Microproc in sites" << endl;
	for (int i = 0; i < nMicropocessiInSiti; i++) {
		std::getline(iffN, line); istringstream iss(line);
		iss >> word; MicropocessiInSiti[i][0] = atoi(word.c_str());
		iss >> word; int MicropocessiInSitisize = atoi(word.c_str()) + 1;
		for (int j = 1; j < MicropocessiInSitisize; j++) {
			iss >> word; MicropocessiInSiti[i].push_back(atoi(word.c_str()));
		}
	}
	std::getline(iffN, line); //fileO << endl;


						 // METRICHE DA MISURARE
	std::getline(iffN, line); //fileO << "Metric to meas " << endl;
	for (int i = 0; i < nMetricheDaMisurare; i++) {
		std::getline(iffN, line); istringstream iss(line);
		iss >> word; MetricheDaMisurare[i][0] = atoi(word.c_str());
		iss >> word; MetricheDaMisurare[i][1] = atoi(word.c_str());
	}

	std::getline(iffN, line); //fileO << endl;


						 // ALMENO N e METRICHE ALMASSIMO 1 METRICHE
	std::getline(iffN, line); //fileO << "At least N " << endl;
	std::getline(iffN, line);
	int count = atoi(line.c_str());
	for (int v = 0; v < count; v++) {
		std::getline(iffN, line); istringstream iss(line);
		iss >> word; int v_temp = atoi(word.c_str());
		iss >> word; int i_temp = atoi(word.c_str());
		iss >> word; DimensionRequests[v_temp][i_temp] = atoi(word.c_str());
	}
	std::getline(iffN, line); //fileO << endl << endl;


	// COSTI
	
	std::getline(iffN, line); //fileO << "Std Costs " << endl;
	std::getline(iffN, line); istringstream iss(line);
	for (int s = 0; s < NSiti; s++) {
		iss >> word; CostoUnitarioSito[s] = atof(word.c_str());
	}
	std::getline(iffN, line); //fileO << endl;
	

	// COSTI VALUTAZIONI
	
	std::getline(iffN, line);//fileO << "Eval Costs " << endl;
	for (int m = 0; m < NMetricMeasurements; m++) {
		std::getline(iffN, line); istringstream iss(line);
		for (int s = 0; s < NSiti; s++) {
			iss >> word; CostoValutazione[m][s] = atof(word.c_str());
		}
	}
	std::getline(iffN, line); //fileO << endl;
	
	std::getline(iffN, line);

	// COSTI IMPLEMENTAZIONE
	
	std::getline(iffN, line);  //fileO << "Impl Costs " << endl;
	for (int m = 0; m < NMetricMeasurements; m++) {
		std::getline(iffN, line); istringstream iss(line);
		for (int s = 0; s < NSiti; s++) {
			iss >> word; CostoSonda[m][s] = atof(word.c_str());
		}
	}

	// KNOWLEDGE BASE
	
	std::getline(iffN, line);  //fileO << "Know Tree " << endl;
	for (int i = 0; i < NDimensioni; i++) {
		std::getline(iffN, line); istringstream iss(line);
		iss >> word; //fileO << i << "  "; // gi� messo in initialize
		iss >> word; int MforDimensionsize = atoi(word.c_str());
		for (int j = 0; j < MforDimensionsize; j++) {
			iss >> word;  MforDimension[i].push_back(atoi(word.c_str()));
		}
	}
	std::getline(iffN, line); //fileO << endl;
	

	for (int i = 0; i < NMetriche; i++) {
		std::getline(iffN, line); istringstream iss(line);
		iss >> word; // fileO << i << "  ||  "; // gi� messo in initialize
		iss >> word; int MMforMetricsize = atoi(word.c_str());
		for (int j = 0; j < MMforMetricsize; j++) {
			iss >> word; MMforMetric[i].push_back(atoi(word.c_str()));
		}
	}
	
	
	std::getline(iffN, line);
	istringstream iss_T(line);
	for (int d = 0; d < NDimensioni; d++) {
		iss_T >> word;
		this->lambdas[d] = atof(word.c_str());
	}

	
	// Numero di istanze per ogni microprocesso
	std::getline(iffN, line);
	istringstream iss_NI(line);
	for (int v = 0; v < NMicroProcessi; v++) {
		iss_NI >> word;
		//cout << word;
		this->numberInstance[v] = atoi(word.c_str());
	}

}

void Instance::generate(bool verbose){

	this->generateKBTree(verbose);

// PRECISIONI
	Precisione = new double**[NMetricMeasurements];
	Delta1Precisione = new double**[NMetricMeasurements];
	Delta2Precisione = new double**[NMetricMeasurements];
	for (int m = 0; m < NMetricMeasurements; m++) {
		Precisione[m] = new double*[NSiti];
		Delta1Precisione[m] = new double*[NSiti];
		Delta2Precisione[m] = new double*[NSiti];
		for (int s = 0; s < NSiti; s++) {
			Precisione[m][s] = new double[NMicroProcessi];
			Delta1Precisione[m][s] = new double[NMicroProcessi];
			Delta2Precisione[m][s] = new double[NMicroProcessi];
			for (int i = 0; i < NMicroProcessi;i++) {
				double temp = (double)rand() / (RAND_MAX);
				if (temp < probPrecisioneZero) {
					Precisione[m][s][i] = 0.0;
					Delta1Precisione[m][s][i] = (maxPrecisione - minPrecisione)*((double)rand() / (RAND_MAX)) + minPrecisione;
				}else {
					Precisione[m][s][i] = (double)rand() / (RAND_MAX);
					Delta1Precisione[m][s][i] = 0.0;
				}
				Delta2Precisione[m][s][i] =  1 - Precisione[m][s][i];
			}
			
		}
	}

	if (verbose) {
		cout << "matrice precisione"<<endl;
		for (int m = 0; m < NMetricMeasurements; m++) {
			for (int s = 0; s < NSiti; s++) {
				for (int i = 0; i < NMicroProcessi; i++) {
					cout << Precisione[m][s][i] << "\t\t";
				}
			}
			cout << endl;
		}
		cout << endl;
		cout << "matrice Delta precisione 1" << endl;
		for (int m = 0; m < NMetricMeasurements; m++) {
			for (int s = 0; s < NSiti; s++) {
				for (int i = 0; i < NMicroProcessi; i++) {
					cout << Delta1Precisione[m][s][i] << "\t\t";
				}
			}
			cout << endl;
		}
		cout << endl;
		cout << "matrice Delta precisione 2" << endl;
		for (int m = 0; m < NMetricMeasurements; m++) {
			for (int s = 0; s < NSiti; s++) {
				for (int i = 0; i < NMicroProcessi; i++) {
					cout << Delta2Precisione[m][s][i] << "\t\t";
				}
			}
			cout << endl;
		}
		cout << endl;

	}

// MICROPROCESSI CHE DEVONO STARE INSIEME OPPURE STACCATI o IN SITI
	if (verbose) cout<<"nCoppieAttivitaSTESSOSito: \t" << nCoppieAttivitaSTESSOSito<<endl;
	if (verbose) cout << "nCoppieAttivitaDIVERSOSito: \t" << nCoppieAttivitaDIVERSOSito << endl<<endl;
	
	// creo una lista di processi tutti diversi:
	vector<int> temp;
	for (int i = 0; i < 2*(nCoppieAttivitaSTESSOSito + nCoppieAttivitaDIVERSOSito) + nMicropocessiInSiti; i++) {
		int candidate = rand() % NMicroProcessi;
		bool flag = false;
		for (int j = 0; j < temp.size(); j++) {
			if (temp[j] == candidate) {
				candidate = (candidate + 1) % (NMicroProcessi - 1);
				j = 0;
			}
		}
		temp.push_back(candidate);
	}

	int count = 0;
	ElencoAttivitaSTESSO = new int*[nCoppieAttivitaSTESSOSito];
	for (int i = 0; i < nCoppieAttivitaSTESSOSito; i++) {
		ElencoAttivitaSTESSO[i] = new int[2];
		for (int j = 0; j < 2; j++) {
			ElencoAttivitaSTESSO[i][j] = temp[count];
			count++;
		}
	}

	ElencoAttivitaDIVERSO = new int*[nCoppieAttivitaDIVERSOSito];
	for (int i = 0; i < nCoppieAttivitaDIVERSOSito; i++) {
		ElencoAttivitaDIVERSO[i] = new int[2];
		for (int j = 0; j < 2; j++) {
			ElencoAttivitaDIVERSO[i][j] = temp[count];
			count++;
		}
	}
	
	MicropocessiInSiti = new vector<int>[nMicropocessiInSiti];
	for (int i = 0; i < nMicropocessiInSiti; i++) {
		MicropocessiInSiti[i].push_back(temp[count]);	// aggiungo un microprocesso dalla lista
														// dalla seconda componente in poi ci sono i siti in cui pu� andare.
		MicropocessiInSiti[i].push_back(rand() % NSiti); // metto il primo sito a caso
		for (int j = 0; j < NSiti; j++)
			if ((double)rand() / (RAND_MAX) < 0.08)
				MicropocessiInSiti[i].push_back(j);

		count++;
	}
		
	if (verbose) {
		cout << "matrice stesso sito" << endl;
		for (int i = 0; i < nCoppieAttivitaSTESSOSito; i++) {
			for (int j = 0; j < 2; j++) {
				cout << ElencoAttivitaSTESSO[i][j] << "\t";
			}
			cout << endl;
		}
		cout << endl;
		cout << "matrice diverso sito" << endl;
		for (int i = 0; i < nCoppieAttivitaDIVERSOSito; i++) {
			for (int j = 0; j < 2; j++) {
				cout << ElencoAttivitaDIVERSO[i][j] << "\t";
			}
			cout << endl;
		}
		cout << endl;
		cout << "microprocessi in siti" << endl;
		for (int i = 0; i < nMicropocessiInSiti; i++) {
			cout << MicropocessiInSiti[i][0] << " || ";
			for (int j = 1; j < MicropocessiInSiti[i].size(); j++) {
				cout << ElencoAttivitaDIVERSO[i][j] << "\t";
			}
			cout << endl;
		}
		cout << endl;
	}

	
// METRICHE DA MISURARE
	if(verbose) cout << "n metriche imposte da misurare: \t" << nMetricheDaMisurare << endl << endl;

	MetricheDaMisurare = new int*[nMetricheDaMisurare];
	for (int i = 0; i < nMetricheDaMisurare; i++) {
		MetricheDaMisurare[i] = new int[2];
		int candidate = rand() % NMetriche;
		for (int j = 0; j < i; j++) {
			if (MetricheDaMisurare[j][0] == candidate) {
				candidate = (candidate + 1) % (NMetriche-1);
				j = 0;
			}
		}
		MetricheDaMisurare[i][0] = candidate;
		MetricheDaMisurare[i][1] = rand() % NMicroProcessi;
	}

	if (verbose) {
		cout << "metriche da misurare:" << endl;
		for (int i = 0; i < nMetricheDaMisurare; i++)
			cout<< MetricheDaMisurare[i][0]<<" sul microprocesso "<< MetricheDaMisurare[i][1] << "\t";
		cout << endl;
	}
	
// ALMENO N e METRICHE ALMASSIMO 1 METRICHE
	DimensionRequests = new int*[NMicroProcessi];
	for (int v = 0; v < NMicroProcessi; v++) {
		DimensionRequests[v] = new int[NDimensioni];
		for (int i = 0; i < NDimensioni; i++) {
			if ((double)rand() / (RAND_MAX) < 0.1) {
				DimensionRequests[v][i] = 0; // niente
			} else {
				if ((double)rand() / (RAND_MAX) < 0.5)
					DimensionRequests[v][i] = -1; // al massimo una
				else
					DimensionRequests[v][i] = rand() % MforDimension[i].size(); // almeno n
			}
		}
	}
	
	if (verbose) {
		for (int v = 0; v < NMicroProcessi; v++) {
			cout << "Richieste per microprocesso "<<v<<": " << endl;
			for (int i = 0; i < NDimensioni; i++) {
				if (DimensionRequests[v][i] == -1) {
					cout << "Dimension " << i << ": " << "Al massimo una" << endl;
				}else if (DimensionRequests[v][i] != 0) {
					cout << "Dimension " << i << ": " << "Almeno " << DimensionRequests[v][i] << endl;
				}
			}
			cout << endl;
		}
		cout << endl << "------" << endl << "------" << endl;
	}
	
// COSTI

	CostoUnitarioSito = new double[NSiti];
	
	//Precisione[m][s];
	//Delta1Precisione[][];
	//Delta2Precisione[][];
	for (int s = 0; s < NSiti; s++) {
		double sum = 0;
		for (int m = 0; m < NMetricMeasurements; m++) 
			for(int i=0; i<=NMicroProcessi;i++)
			sum += Precisione[m][s][i];
		
		sum = sum / NMetricMeasurements;
		double temp = (costoMax - costoMin)*(sum + 0.2*(double)rand() / (RAND_MAX)-0.1) + costoMin;
		if (temp > costoMax) temp = costoMax;
		if (temp < costoMin) temp = costoMin;
		CostoUnitarioSito[s] = temp;
	} 



	CostoValutazione = new double*[NMetricMeasurements];
	CostoSonda = new double*[NMetricMeasurements];
	for (int m = 0; m < NMetricMeasurements; m++) {
		CostoValutazione[m] = new double[NSiti];
		CostoSonda[m] = new double[NSiti];
		for (int s = 0; s < NSiti; s++) {
			CostoValutazione[m][s] = costoMaxVal; //(costoMaxVal-costoMinVal)*((double)rand() / (RAND_MAX))+costoMinVal;
			CostoSonda[m][s] = costoMaxSonda; //(costoMaxSonda - costoMinSonda)*((double)rand() / (RAND_MAX)) + costoMinSonda;
		}
	}

	if (verbose) {
		cout << "Costo unitario sito" << endl;
		for (int s = 0; s < NSiti; s++) {
			cout << CostoUnitarioSito[s] << "\t";
		}
		cout << endl;

		cout << "Costo valutazione MM" << endl;
		for (int m = 0; m < NMetricMeasurements; m++) {
			for (int s = 0; s < NSiti; s++) {
				cout<<CostoValutazione[m][s] << "\t";
			}
			cout << endl;
		}
		cout << endl;

		cout << "Costo sonda" << endl;
		for (int m = 0; m < NMetricMeasurements; m++) {
			for (int s = 0; s < NSiti; s++) {
				cout << CostoSonda[m][s] << "\t";
			}
			cout << endl;
		}
		cout << endl;	
	}
	
	
}

//------------------------------------------------------------------------------->
void Instance::printInstance(string path, int index) {
	ostringstream ss;

	ss << path << "/Instance";
	if (index >= 0) {
		ss << index << ".txt";
	}
	else {
		ss << ".txt";
	}
	ofstream fileO(ss.str().c_str());

	fileO << "General Coeff" << endl;
	fileO << this->NDimensioni << "  " << this->NMetriche << " " << this->NMetricMeasurements << " " << this->NMicroProcessi << " " << NSiti << endl;
	fileO << nCoppieAttivitaSTESSOSito << "  " << nCoppieAttivitaDIVERSOSito << "  " << nMicropocessiInSiti << "  " << nMetricheDaMisurare << endl;
	// fileO << nIstanzeMicroprocessi <<" "<< budget <<" "<<minPrecisione<<endl;
	fileO << budget << " " << minPrecisione << endl;
	fileO << endl;

	//this->generateKBTree(verbose);

// PRECISIONI
//std
	fileO << "Std Prec" << endl;
	fileO << endl;
	for (int v = 0; v < NMicroProcessi; v++) {
		for (int m = 0; m < NMetricMeasurements; m++) {
			for (int s = 0; s < NSiti; s++) {
				fileO << Precisione[m][s][v] << "\t\t";
			}
			fileO << endl;
		}
		fileO << endl;
	}
	fileO << endl;

	//stima
	fileO << "Stim Prec" << endl;
	fileO << endl;
	for (int v = 0; v < NMicroProcessi; v++) {
		for (int m = 0; m < NMetricMeasurements; m++) {
			for (int s = 0; s < NSiti; s++) {
				fileO << Delta1Precisione[m][s][v] << "\t\t";
			}
			fileO << endl;
		}
		fileO << endl;
	}
	fileO << endl;
	//make	
	fileO << "Stim Meas" << endl;
	fileO << endl;
	for (int v = 0; v < NMicroProcessi; v++) {
		for (int m = 0; m < NMetricMeasurements; m++) {
			for (int s = 0; s < NSiti; s++) {
				fileO << Delta2Precisione[m][s][v] << "\t\t";
			}
			fileO << endl;
		}
		fileO << endl;
	}
	fileO << endl;

// MICROPROCESSI CHE DEVONO STARE INSIEME OPPURE STACCATI o IN SITI
	fileO << "Constr Same Site" << endl;
	for (int i = 0; i < nCoppieAttivitaSTESSOSito; i++) {
		for (int j = 0; j < 2; j++) {
			fileO << ElencoAttivitaSTESSO[i][j] << "\t";
		}
		fileO << endl;
	}
	fileO << endl;

	fileO << "Constr Different Site" << endl;
	for (int i = 0; i < nCoppieAttivitaDIVERSOSito; i++) {
		for (int j = 0; j < 2; j++) {
			fileO << ElencoAttivitaDIVERSO[i][j] << "\t";
		}
		fileO << endl;
	}
	fileO << endl;

	
	fileO << "Constr Microproc in sites" << endl;
	for (int i = 0; i < nMicropocessiInSiti; i++) {
		fileO << MicropocessiInSiti[i][0] << "\t";
		fileO << MicropocessiInSiti[i].size()-1 << "\t";
		for (int j = 1; j < MicropocessiInSiti[i].size(); j++) {
			fileO << MicropocessiInSiti[i][j] << "\t";
		}
		fileO << endl;
	}
	fileO << endl;
	
	
	
// METRICHE DA MISURARE
	fileO << "Metric to meas " << endl;
	for (int i = 0; i < nMetricheDaMisurare; i++)
		fileO << MetricheDaMisurare[i][0] << " " << MetricheDaMisurare[i][1] <<endl; // nMetrica -- id MicroPorcesso
	fileO << endl;
	
	
	// ALMENO N e METRICHE ALMASSIMO 1 METRICHE
	fileO << "At least N " << endl;
	int count = 0;
	for (int v = 0; v < NMicroProcessi; v++) {
		for (int d = 0; d < NDimensioni; d++) {
			if (DimensionRequests[v][d] == -1)		count++;
			else if (DimensionRequests[v][d] != 0) 	count++;
		}
	}
	fileO << count << endl;
	for (int v = 0; v < NMicroProcessi; v++) {
		for (int d = 0; d < NDimensioni; d++) {
			if (DimensionRequests[v][d] == -1)		fileO << v <<" " << d << " "<< -1 << endl;
			else if (DimensionRequests[v][d] != 0) 	fileO << v << " " << d << " " << DimensionRequests[v][d] << endl;
		}
	}
	fileO << endl;
	

// COSTI
	fileO << "Std Costs " << endl;
	for (int s = 0; s < NSiti; s++) {
		fileO << CostoUnitarioSito[s] << "\t";
	}
	fileO << endl<<endl;


// COSTI VALUTAZIONI
	fileO << "Eval Costs " << endl;
	for (int m = 0; m < NMetricMeasurements; m++) {
		for (int s = 0; s < NSiti; s++) {
			fileO << CostoValutazione[m][s] << "\t";
		}
		fileO << endl;
	}
	fileO << endl;


// COSTI IMPLEMENTAZIONE
	fileO << "Impl Costs " << endl;
	for (int m = 0; m < NMetricMeasurements; m++) {
		for (int s = 0; s < NSiti; s++) {
			fileO << CostoSonda[m][s] << "\t";
		}
		fileO << endl;
	}
	fileO << endl;

	
// KNOWLEDGE BASE
	fileO << "Know Tree " << endl;
	for (int i = 0; i < NDimensioni; i++) {
		fileO << i << "\t "<< MforDimension[i].size()<<"\t ";
		for (int j = 0; j < MforDimension[i].size(); j++) {
			fileO << MforDimension[i][j] << "\t";
		}
		fileO << endl;
	}
	fileO << endl;
	for (int i = 0; i < NMetriche; i++) {
		fileO << i << "\t "<< MMforMetric[i].size()<<"\t ";
		for (int j = 0; j < MMforMetric[i].size(); j++) {
			fileO << MMforMetric[i][j] << "\t";
		}
		fileO << endl;
	}
	

	//lambda
	for (int d = 0; d < NDimensioni; d++) {
		fileO << this->lambdas[d] << "  ";
	}
	fileO << endl;
	// numberInstance

	for (int v = 0; v < NMicroProcessi; v++) {
		fileO << this->numberInstance[v] << "  ";
	}
	fileO << endl;

	

	fileO.close();
}
//------------------------------------------------------------------------------->

double Instance::solveTest(int nScenarios, string path, int i, int extracost, bool verbose) {
	try {
		GRBEnv env = GRBEnv();
		GRBModel model = GRBModel(env);

		// x{vs}: microprocesso v nel sito s
		GRBVar** x = new GRBVar*[NMicroProcessi];
		for (int v = 0; v < NMicroProcessi; v++) {
			x[v] = new GRBVar[NSiti];
			for (int s = 0; s < NSiti; s++) {
				stringstream ss;
				ss << "x" << v << "_" << s;
				string name = ss.str();
				x[v][s] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
			}
		}

		// z_{ms}^v: investo in una stima della metric meaurement m nel sito s quando � presenteil microprocesso v
		GRBVar*** z = new GRBVar**[NMetricMeasurements];
		for (int mm = 0; mm < NMetricMeasurements; mm++) {
			z[mm] = new GRBVar*[NSiti];
			for (int s = 0; s < NSiti; s++) {
				z[mm][s] = new GRBVar[NMicroProcessi];
				for (int v = 0; v < NMicroProcessi; v++) {
					stringstream ss;
					ss << "z_" << mm << "_" << s << "_" << v;
					string name = ss.str();
					z[mm][s][v] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
				}
			}
		}



		// y_{ms}^v: investo in una sonda per la metric meaurement m nel sito s quando � presenteil microprocesso v
		GRBVar*** y = new GRBVar**[NMetricMeasurements];
		for (int mm = 0; mm < NMetricMeasurements; mm++) {
			y[mm] = new GRBVar*[NSiti];
			for (int s = 0; s < NSiti; s++) {
				y[mm][s] = new GRBVar[NMicroProcessi];
				for (int v = 0; v < NMicroProcessi; v++) {
					stringstream ss;
					ss << "y_" << mm << "_" << s << "_" << v;
					string name = ss.str();
					y[mm][s][v] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
				}
			}
		}



		// w_{Mv}: stimo una MM della mtrica M per il  microprocesso v
		GRBVar** w = new GRBVar*[NMetriche];
		for (int m = 0; m < NMetriche; m++) {
			w[m] = new GRBVar[NMicroProcessi];
			for (int v = 0; v < NMicroProcessi; v++) {
				stringstream ss;
				ss << "w_" << m << "_" << v;
				string name = ss.str();
				w[m][v] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
			}
		}

		// l: extracost
		GRBVar l = model.addVar(0, DBL_MAX, 0, GRB_CONTINUOUS, "l");


		// Integrate variables into model
		model.update();

		GRBLinExpr obj = 0;
		GRBLinExpr expr = 0;


		obj = 0;
		for (int d = 0; d < NDimensioni; d++) {
			for (int M = 0; M < MforDimension[d].size(); M++) {
				int idMetrica = MforDimension[d][M];
				for (int mm = 0; mm < MMforMetric[idMetrica].size(); mm++) {
					int idMM = MMforMetric[idMetrica][mm];
					//cout << idMM << endl;
					for (int s = 0; s < NSiti; s++)
						for (int v = 0; v < NMicroProcessi; v++) {
							obj += lambdas[d] * (Precisione[idMM][s][v] * x[v][s] + Delta1Precisione[idMM][s][v] * z[idMM][s][v] + Delta2Precisione[idMM][s][v] * y[idMM][s][v]);	
						}
				}
			}
		}

		obj += -0.1*l / budget;

		model.setObjective(obj, GRB_MAXIMIZE); //devo massimizzare e non minimizzare quindi metto il meno
// ADD CONSTRAINTS
											   // Ogni microservizio va deploiato
		for (int v = 0; v < NMicroProcessi; v++) {
			expr = 0;
			for (int s = 0; s < NSiti; s++)
				expr += x[v][s];
			model.addConstr(expr, GRB_EQUAL, 1);
		}


		// non posso stimare o mettere una sonda se non ho il messo prima il processo
		for (int mm = 0; mm < NMetricMeasurements; mm++)
			for (int s = 0; s < NSiti; s++)
				for (int v = 0; v < NMicroProcessi; v++)
					model.addConstr(y[mm][s][v] + z[mm][s][v], GRB_LESS_EQUAL, x[v][s]);

		// stimo una MM solo se non c'� nel sito
		for (int mm = 0; mm < NMetricMeasurements; mm++)
			for (int s = 0; s < NSiti; s++)
				for (int v = 0; v < NMicroProcessi; v++)
					model.addConstr(Precisione[mm][s][v], GRB_LESS_EQUAL, 1 - z[mm][s][v]);

		// VINCOLI 5,6,7,8,9 E 10
		if (NMicroProcessi >= 10 && NSiti >= 10 && NMetricMeasurements >= 15) {

			//  Vincolo su attivit� nello stesso sito
			for (int i = 0; i < nCoppieAttivitaSTESSOSito; i++) {
				for (int s = 0; s < NSiti; s++)
					model.addConstr(x[ElencoAttivitaSTESSO[i][0]][s], GRB_EQUAL, x[ElencoAttivitaSTESSO[i][1]][s]);
			}


			//  Vincolo su attivit� in siti diversi siti
			for (int i = 0; i < nCoppieAttivitaDIVERSOSito; i++) {
				for (int s = 0; s < NSiti; s++)
					model.addConstr(x[ElencoAttivitaDIVERSO[i][0]][s] + x[ElencoAttivitaSTESSO[i][1]][s], GRB_LESS_EQUAL, 1);
			}


			// Vincolo di un sito in un set di siti
			for (int i = 0; i < nMicropocessiInSiti; i++) {
				int indexMP = MicropocessiInSiti[i][0]; // index microprocesso
				expr = 0;
				for (int s = 1; s < MicropocessiInSiti[i].size(); s++) {
					//cout << indexMP << " " << MicropocessiInSiti[i][s] << endl;
					expr += x[indexMP][MicropocessiInSiti[i][s]];
				}
				model.addConstr(expr, GRB_GREATER_EQUAL, 1);
			}

			// Devo misurare alcune metriche per alcuni microprocessi.
			for (int i = 0; i < nMetricheDaMisurare; i++) {
				model.addConstr(w[MetricheDaMisurare[i][0]][MetricheDaMisurare[i][1]], GRB_EQUAL, 1);
			}

			// per una particolare dimensione ne devo prendere almeno n
			// DimensionRequests

			for (int v = 0; v < NMicroProcessi; v++) {
				for (int d = 0; d < NDimensioni; d++) {
					if (DimensionRequests[v][d] > 0) {
						expr = 0;
						for (int m = 0; m < MforDimension[d].size(); m++)
							expr += w[m][d];
						model.addConstr(expr, GRB_GREATER_EQUAL, DimensionRequests[v][d]);
						//if(DimensionRequests[v][d] > MforDimension[d].size()) cout<<"MODEL INFEASIBLE"<<endl;
					}

				}
			}

		}
		// FINE VINCOLI 5,6,7,8,9 E 10 
		/**/


		// Precision
		for (int v = 0; v < NMicroProcessi; v++) { // per ogni microservizio
			for (int d = 0; d < NDimensioni; d++) {
				for (int M = 0; M < MforDimension[d].size(); M++) {
					expr = 0;
					int idMetrica = MforDimension[d][M];
					for (int mm = 0; mm < MMforMetric[idMetrica].size(); mm++) {
						int idMM = MMforMetric[idMetrica][mm];
						for (int s = 0; s < NSiti; s++)
							expr += Precisione[idMM][s][v] * x[v][s] + Delta1Precisione[idMM][s][v] * z[idMM][s][v] + Delta2Precisione[idMM][s][v] * y[idMM][s][v];
					}
					model.addConstr(minPrecisione*w[idMetrica][v], GRB_LESS_EQUAL, expr);
				}
			}
		}

		// Costo
		expr = 0;
		for (int s = 0; s < NSiti; s++)
			for (int v = 0; v < NMicroProcessi; v++)
				expr += CostoUnitarioSito[s] * numberInstance[v] * x[v][s];
		for (int mm = 0; mm < NMetricMeasurements; mm++)
			for (int s = 0; s < NSiti; s++)
				for (int v = 0; v < NMicroProcessi; v++)
					expr += CostoValutazione[mm][s] * z[mm][s][v] + CostoSonda[mm][s] * y[mm][s][v];


		model.addConstr(expr, GRB_LESS_EQUAL, l);

		// costo totale
		model.addConstr(l, GRB_LESS_EQUAL, budget);
		
		if (!verbose) model.getEnv().set(GRB_IntParam_OutputFlag, 0);

		clock_t timeStartMethod = clock();
		model.optimize();
		
		for (int v = 0; v < NMicroProcessi; v++)
			for (int s = 0; s < NSiti; s++) 
				sol.x[v][s] = x[v][s].get(GRB_DoubleAttr_X);
		for (int mm = 0; mm < NMetricMeasurements; mm++)
			for (int s = 0; s < NSiti; s++) 
				for (int v = 0; v < NMicroProcessi; v++) {
					sol.z[mm][s][v] = z[mm][s][v].get(GRB_DoubleAttr_X);
					sol.y[mm][s][v] = y[mm][s][v].get(GRB_DoubleAttr_X);
				}
		for (int m = 0; m < NMetriche; m++)
			for (int v = 0; v < NMicroProcessi; v++) 
				sol.w[m][v] = w[m][v].get(GRB_DoubleAttr_X);
			




		hasSolution = true;
		this->testSolutionInStochastic(nScenarios, path, i);
		objFunction = model.get(GRB_DoubleAttr_ObjVal) + 0.01*l.get(GRB_DoubleAttr_X) / budget;
	}
	catch (GRBException e) {
		cout << "Error code = " << e.getErrorCode() << endl;
		cout << e.getMessage() << endl;
		cin.get();
		exit(1);
	}
	catch (...) {
		cout << "Error during optimization" << endl;
		cin.get();
		exit(1);
	}
	return objFunction;
}

void Instance::modifyMeanwithQuantile(double quantile) {
	for (int v = 0; v < NMicroProcessi; v++) {
		numberInstance[v] = quantileMicroprocess(quantile, v);
	}
		
}

int Instance::quantileMicroprocess(double quantile, int nMP) {
	double sum = 0.0; // probNInstancePMF[nMP][0];
	
	// if (sum < probNInstancePMF[nMP][0]) return 0.0;
	
	for (int j = 0; j < nIstanzeMicroprocessi; j++) {
		sum += probNInstancePMF[nMP][j];
		if (sum > quantile)
			return j;
	}
	return nIstanzeMicroprocessi;
}

void Instance::setGradimentoMetriche(vector<double> Gradimento){
	//this->GradimentoMetriche = Gradimento;
}

void Instance::generateKBTree(bool verbose) {

	if ( NMetriche<NDimensioni ||  NMetricMeasurements < NMetriche) {
		cout << "Impossibile generare Knowledge Base" << endl;
		cin.get();
		exit(1);
	}

	srand(time(NULL));


	MforDimension = new vector<int>[NDimensioni];
	// poich� non voglio avere dimensioni vuote faccio un primo giro in cui scrivo qualcosa
	for (int i = 0; i < NDimensioni; i++) {
		MforDimension[i].push_back(i);
	}
	// poi continuo ad aggiungere
	for (int i = NDimensioni; i < NMetriche; i++) {
		int pos = rand() % NDimensioni;
		MforDimension[pos].push_back(i);
		double temp = (double)rand() / (RAND_MAX);
		if (temp < probOverlapM) {
			pos = (pos + 1) % NDimensioni;
			MforDimension[pos].push_back(i);
		}
	}
	if (verbose) {
		cout << "Dimensioni:" << endl;
		for (int i = 0; i < NDimensioni; i++) {
			cout << i << "  ||  ";
			for (int j = 0; j < MforDimension[i].size(); j++) {
				cout << MforDimension[i][j] << "\t";
			}
			cout << endl;
		}
	}


	// Generazione MetricMeasurement per ogni metrica

	MMforMetric = new vector<int>[NMetriche];
	// poich� non voglio avere metriche vuote faccio un primo giro in cui scrivo qualcosa
	for (int i = 0; i < NMetriche; i++) {
		MMforMetric[i].push_back(i);
	}
	// poi continuo ad aggiungere
	for (int i = NMetriche; i < NMetricMeasurements; i++) {
		int pos = rand() % NMetriche;
		MMforMetric[pos].push_back(i);
		double temp = (double)rand() / (RAND_MAX);
		if (temp < probOverlapMM) {
			pos = (pos + 1) % NMetriche;
			MMforMetric[pos].push_back(i);
		}
	}
	if (verbose) {
		cout << "Metriche:" << endl;
		for (int i = 0; i < NMetriche; i++) {
			cout << i << "  ||  ";
			for (int j = 0; j < MMforMetric[i].size(); j++) {
				cout << MMforMetric[i][j] << "\t";
			}
			cout << endl;
		}
	}
}

void Instance::testSolutionInStochastic(int nScenarios, string path,int i) {
	if (!hasSolution) {
		cout << "Impossible to test, there is no solution ";
		return;
	}
	
	ostringstream oss;
	if (i >= 0) { oss << path << "/Scenario" << i << ".txt";
	}else { oss << path << "/Scenario.txt"; }

	ofstream fileO(oss.str().c_str());

	// nel caso stocastico quello che cambia � il numero
	double expectation = 0.0;
	double variance = 0.0;
	double* realizations = new double[NMicroProcessi];

	double costoFisso = 0.0;
	for (int mm = 0; mm < NMetricMeasurements; mm++)
		for (int s = 0; s < NSiti; s++) {
			for (int v = 0; v < NMicroProcessi; v++) {
				if (sol.z[mm][s][v] > 0.5)
					costoFisso += CostoValutazione[mm][s];
				if (sol.y[mm][s][v] > 0.5)
					costoFisso += CostoSonda[mm][s];
			}
		}

	//nScenarios = 1;/*****************************/
	
	//fileO << "x=[" << endl;
	for (int i = 0; i < nScenarios; i++) {
		double costo = 0.0;

		for (int v = 0; v < NMicroProcessi; v++) {
			realizations[v] = realizationMicroprocess(v);
			//cout << realizations[v] << endl;/*****************************/
		}
		for (int s = 0; s < NSiti; s++)
			for (int v = 0; v < NMicroProcessi; v++)
				if (sol.x[v][s]>0.5)
					costo += CostoUnitarioSito[s] * realizations[v];
		costo+= costoFisso;

		expectation += costo;
		variance += pow(costo, 2.0);

		fileO << costo << endl;
	}
	//fileO << "];" << endl;
	expectation = expectation / nScenarios;
	variance = variance / nScenarios - pow(expectation, 2.0);
	cout << expectation << " " << variance << endl;
	fileO.close();
}

int Instance::realizationMicroprocess(int nMP){
	
	double temp = (double)rand() / (RAND_MAX);
	
	double cum = 0.0;
	double cum1 = 0.0;
	if (temp < probNInstancePMF[nMP][0])
		return 0.0;
	for (int j = 1; j < nIstanzeMicroprocessi - 1; j++) {
		cum+= probNInstancePMF[nMP][j];
		cum1 = cum + probNInstancePMF[nMP][j + 1];
		if (temp>cum && temp<cum1) {
			return j;
		}
	}

	return nIstanzeMicroprocessi;
}

void Instance::dumpKBTree(string path) {

	ofstream fileO(string(path).append("/KBTree.html").c_str());

	fileO<<"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\">"<<endl;
	fileO<<"<html lang=\"en\">"<<endl;
	fileO<<"<head>"<<endl;
	fileO<<"	<meta charset=\"utf-8\">"<<endl;
	fileO<<"	<title>Knowledge Base</title>"<<endl;
	fileO<<"	<script type=\"text/javascript\" src=\"http://mbostock.github.com/d3/d3.js?2.6.0\"></script>"<<endl;
	fileO<<"	<script type=\"text/javascript\" src=\"http://mbostock.github.com/d3/d3.layout.js?2.6.0\"></script>"<<endl;
	fileO<<"	<script type=\"text/javascript\" src=\"http://mbostock.github.com/d3/d3.geom.js?2.6.0\"></script>"<<endl;
	fileO<<"</head>"<<endl;
	fileO<<"<body>"<<endl;
	fileO<<"	<script type=\"text/javascript\" charset=\"utf-8\">"<<endl;
	fileO<<"		var w = 2000, h = 1000;"<<endl; 
	fileO<<"		var labelDistance = 0;"<<endl;
	fileO<<"		var vis = d3.select(\"body\").append(\"svg:svg\").attr(\"width\", w).attr(\"height\", h);"<<endl;

	fileO<<"		var nodes = [];"<<endl;
	fileO<<"		var labelAnchors = [];"<<endl;
	fileO<<"		var labelAnchorLinks = [];"<<endl;
	fileO<<"		var links = [];"<<endl;


	for (int d = 0; d < NDimensioni; d++) {
		fileO << "		var node"<<d<<" = {label : \"d \" + "<<d<<"	};" << endl;
		fileO << "		nodes.push(node" << d << ");" << endl;
		fileO << "		labelAnchors.push({	node : node" << d << "});" << endl;
		fileO << "		labelAnchors.push({	node : node" << d << "});" << endl;
	}

	for (int m = 0; m < NMetriche; m++) {
		fileO << "		var node" << m << " = {label : \"M \" + " << m << "	};" << endl;
		fileO << "		nodes.push(node" << m << ");" << endl;
		fileO << "		labelAnchors.push({	node : node" << m << "});" << endl;
		fileO << "		labelAnchors.push({	node : node" << m << "});" << endl;
	}

	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		fileO << "		var node" << mm << " = {label : \"mm \" + " << mm << "	};" << endl;
		fileO << "		nodes.push(node" << mm << ");" << endl;
		fileO << "		labelAnchors.push({	node : node" << mm << "});" << endl;
		fileO << "		labelAnchors.push({	node : node" << mm << "});" << endl;
	}

	for (int i = 0; i < NDimensioni; i++) {
		for (int j = 0; j < MforDimension[i].size(); j++) {
			fileO << "		links.push({source : " << i << ",	target : " <<NDimensioni+MforDimension[i][j] << ",	weight : 1});" << endl;
		}
	}

	for (int i = 0; i < NMetriche; i++) {
		for (int j = 0; j < MMforMetric[i].size(); j++) {
			fileO << "		links.push({source : " << NDimensioni + i << ",	target : " << NDimensioni+NMetriche + MMforMetric[i][j] << ",	weight : 1});" << endl;
		}
	}

	fileO<<"		for(var i = 0; i < nodes.length; i++) {"<<endl;
	fileO<<"			labelAnchorLinks.push({"<<endl;
	fileO<<"			source : i * 2,"<<endl;
	fileO<<"			target : i * 2 + 1,"<<endl;
	fileO<<"			weight : 1"<<endl;
	fileO<<"		});"<<endl;
	fileO<<"		};"<<endl;

	fileO<<"		var force = d3.layout.force().size([w, h]).nodes(nodes).links(links).gravity(1).linkDistance(50).charge(-3000).linkStrength(function(x) {"<<endl;
	fileO<<"			return x.weight * 10"<<endl;
	fileO<<"		});"<<endl;


	fileO<<"		force.start();"<<endl;

	fileO<<"		var force2 = d3.layout.force().nodes(labelAnchors).links(labelAnchorLinks).gravity(0).linkDistance(0).linkStrength(8).charge(-100).size([w, h]);"<<endl;
	fileO<<"		force2.start();"<<endl;

	fileO<<"		var link = vis.selectAll(\"line.link\").data(links).enter().append(\"svg:line\").attr(\"class\", \"link\").style(\"stroke\", \"#CCC\");"<<endl;

	fileO<<"		var node = vis.selectAll(\"g.node\").data(force.nodes()).enter().append(\"svg:g\").attr(\"class\", \"node\");"<<endl;
	fileO<<"		node.append(\"svg:circle\").attr(\"r\", 5).style(\"fill\", \"#555\").style(\"stroke\", \"#FFF\").style(\"stroke-width\", 3);"<<endl;
	fileO<<"		node.call(force.drag);"<<endl;


	fileO<<"		var anchorLink = vis.selectAll(\"line.anchorLink\").data(labelAnchorLinks)//.enter().append(\"svg:line\").attr(\"class\", \"anchorLink\").style(\"stroke\", \"#999\");"<<endl;

	fileO<<"		var anchorNode = vis.selectAll(\"g.anchorNode\").data(force2.nodes()).enter().append(\"svg:g\").attr(\"class\", \"anchorNode\");"<<endl;
	fileO<<"		anchorNode.append(\"svg:circle\").attr(\"r\", 0).style(\"fill\", \"#FFF\");"<<endl;
	fileO<<"		anchorNode.append(\"svg:text\").text(function(d, i) {"<<endl;
	fileO<<"		return i % 2 == 0 ? \"\" : d.node.label"<<endl;
	fileO<<"		}).style(\"fill\", \"#555\").style(\"font-family\", \"Arial\").style(\"font-size\", 12);"<<endl;

	fileO<<"		var updateLink = function() {"<<endl;
	fileO<<"			this.attr(\"x1\", function(d) {"<<endl;
	fileO<<"			return d.source.x;"<<endl;
	fileO<<"		}).attr(\"y1\", function(d) {"<<endl;
	fileO<<"			return d.source.y;"<<endl;
	fileO<<"		}).attr(\"x2\", function(d) {"<<endl;
	fileO<<"			return d.target.x;"<<endl;
	fileO<<"		}).attr(\"y2\", function(d) {"<<endl;
	fileO<<"			return d.target.y;"<<endl;
	fileO<<"		});"<<endl;
	fileO<<"		}"<<endl;
	fileO<<"		var updateNode = function() {"<<endl;
	fileO<<"		this.attr(\"transform\", function(d) {"<<endl;
	fileO<<"		return \"translate(\" + d.x + \",\" + d.y + \")\";"<<endl;
	fileO<<"		});"<<endl;
	fileO<<"		}"<<endl;
	fileO<<"		force.on(\"tick\", function() {"<<endl;
	fileO<<"		force2.start();"<<endl;
	fileO<<"		node.call(updateNode);"<<endl;
	fileO<<"		anchorNode.each(function(d, i) {"<<endl;
	fileO<<"			if(i % 2 == 0) {"<<endl;
	fileO<<"				d.x = d.node.x;"<<endl;
	fileO<<"				d.y = d.node.y;"<<endl;
	fileO<<"			} else {"<<endl;
	fileO<<"				var b = this.childNodes[1].getBBox();"<<endl;
	fileO<<"				var diffX = d.x - d.node.x;"<<endl;
	fileO<<"				var diffY = d.y - d.node.y;"<<endl;
	fileO<<"				var dist = Math.sqrt(diffX * diffX + diffY * diffY);"<<endl;
	fileO<<"				var shiftX = b.width * (diffX - dist) / (dist * 2);"<<endl;
	fileO<<"				shiftX = Math.max(-b.width, Math.min(0, shiftX));"<<endl;
	fileO<<"				var shiftY = 5;"<<endl;
	fileO<<"				this.childNodes[1].setAttribute(\"transform\", \"translate(\" + shiftX + \",\" + shiftY + \")\");"<<endl;
	fileO<<"			}"<<endl;
	fileO<<"		});"<<endl;
	fileO<<"		anchorNode.call(updateNode);"<<endl;
	fileO<<"		link.call(updateLink);"<<endl;
	fileO<<"		anchorLink.call(updateLink);"<<endl;
	fileO<<"		});"<<endl;
	fileO<<"	</script>"<<endl;
	fileO<<"	</body>"<<endl;
	fileO<<"</html>"<<endl;
	
	fileO.close();
}

void Instance::dumpSolution(string path) {
	if (!hasSolution) {
		cout << "Nessuna soluzione da stampare" << endl;
		return;
	}

	ofstream fileO(string(path).append("/solution.html").c_str());
	fileO << "<body>" << endl;
	//ofstream fileO("../Cluster/soluzione.txt");
	fileO << "Accuracy: " << this->solutionAccuracy() <<"  ||  Cost: "<< this->solutionCost() <<"<br/>" << "<br/>" << endl;

	for (int s = 0; s<NSiti; s++) {
		
		bool flag =  false;
		for (int v = 0; v< NMicroProcessi; v++) {
			if (sol.x[v][s] > 0.5) {
				flag = true;
			}
		}
		if (flag) {
			fileO << "<div style=\"clear: left;\">" << endl;
			fileO << "SITO " << s << "<br/>" << endl;
			fileO << "\t Microprocessi':" << "<br/>" << endl;
			//fileO << "\t" ;
			for (int v = 0; v < NMicroProcessi; v++) {
				if (sol.x[v][s]> 0.5) {
					fileO << "\t" << v << "||  ";
				}
			}
			fileO<<"<br/>" << endl;
			fileO << "<div style=\" float:right; width:50%;\">" << endl;
			fileO << "\t metriche stimate:" << "<br/>" << endl;
			int NStimate = 0;
			for (int mm = 0; mm < NMetricMeasurements; mm++) {
				bool flag = false;
				bool* who = new bool[NMicroProcessi];
				for (int v = 0; v < NMicroProcessi; v++)
					if (sol.z[mm][s][v]> 0.5) {
						flag = true;
						who[v] = true;
					}
					else {
						who[v] = false;
					}
				if (flag) {
					fileO << "\t" << mm << endl;
					fileO << "(  ";
					for (int v = 0; v < NMicroProcessi; v++)
						if (who[v])
							fileO << v << " ";
					fileO << "  )";
					fileO << "<br/>" << endl;
					NStimate++;
				}
			}
			if (NStimate == 0) {
				fileO << "\t ----";
			}
			fileO << " tot Stimate: "<<NStimate<<" <br/></div>" << endl;
			fileO << "<div style=\" float:left; width:50%;\">" << endl;
			fileO << "\t sonde implementate:" << "<br/>" << endl;
			int NSonde = 0;
			for (int mm = 0; mm < NMetricMeasurements; mm++) {
				bool flag = false;
				bool* who = new bool[NMicroProcessi];
				for (int v = 0; v < NMicroProcessi; v++)
					if (sol.y[mm][s][v] > 0.5) {
						flag = true;
						who[v] = true;
					}else {
						who[v] = false;
					}
				if (flag) {
					fileO << "\t" << mm << endl;
					fileO << "(  ";
					for (int v = 0; v < NMicroProcessi; v++)
						if (who[v])
							fileO << v << " ";
					fileO << "  )";
					fileO << "<br/>" <<endl;
					NSonde++;
				}
			}

			if (NSonde == 0) {
				fileO << "\t ----";
			}

			fileO << " tot Probes: " << NSonde<< "<br/></div>" << endl;
			fileO << "</div>" << endl;
			fileO << "<br/><br/> <hr> <br/><br/>" << endl;
		}
	}

	fileO << "Costo soluzione: " << "<br/>" << endl;
	fileO << "Std (moltiplicato per il numero di istanze): ";
	double costoStd = 0;
	for (int s = 0; s < NSiti; s++) {
		double parziale = 0.0;
		for (int v = 0; v < NMicroProcessi; v++) {
			if (sol.x[v][s] > 0.5) {
				costoStd += numberInstance[v] * CostoUnitarioSito[s];
				parziale += numberInstance[v] * CostoUnitarioSito[s];
			}
		}
		fileO << parziale << "(" << s << ") ";
	}

	fileO<<" = " << costoStd << "<br/>" << endl;
	fileO << endl << "Stime: ";
	double costoEst = 0;
	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		for (int s = 0; s < NSiti; s++) {
			for (int v = 0; v < NMicroProcessi; v++) {
				if (sol.z[mm][s][v] > 0.5) {
					costoEst += CostoValutazione[mm][s];
				}
			}
		}
	}
	fileO<<" = " << costoEst << endl;
	fileO << "<br/>" << endl << "Sonde: ";

	double costoProb = 0;
	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		for (int s = 0; s < NSiti; s++) {
			for (int v = 0; v < NMicroProcessi; v++) {
				if (sol.y[mm][s][v] > 0.5) {
					costoProb += CostoSonda[mm][s];
					//fileO << CostoSonda[mm][s] << "(" << mm << "--" << s << ") ";
				}
			}
		}
	}
	fileO<<" = " << costoProb << "<br/>" << endl;
	fileO << "<br/>" << endl << "Costo Totale: " << costoStd + costoEst + costoProb<<endl;// << " quindi l e' uguale a : " << l.get(GRB_DoubleAttr_X) << endl;
	fileO << "</body>" << endl;
	fileO.close();
}

void Instance::dumpInstance(string path){
	
	ostringstream oss;

	oss << path << "/Instance.html";

	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "	<head>" << endl;
	fileO << "		<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << "		<script type=\"text/javascript\">" << endl;
	fileO << "			google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	fileO << "			google.setOnLoadCallback(drawScenario);" << endl;
	fileO << "			google.setOnLoadCallback(drawMarket);" << endl;
	fileO << "			google.setOnLoadCallback(drawStates);" << endl;

	
	// carica distribuzione
	for (int i = 0; i < NMicroProcessi; i++) {
		fileO << "function loadDistribution" << i << "(){" << endl;
		fileO << "	document.getElementById(\"content\").innerHTML='<object type=\"text/html\" data=";
		fileO <<"\"Data/Distribution" << i << ".html\" style=\"width: 100%;height: 100%\"></object>';" << endl;
		fileO << "} " << endl;
	}

	// carica siti
	for (int i = 0; i < NSiti; i++) {
		fileO << "function loadSito" << i << "(){" << endl;
		fileO << "	document.getElementById(\"content\").innerHTML='<object type=\"text/html\" data=";
		fileO << "\"Data/Sito" << i << ".html\" style=\"width: 100%;height: 100%\"></object>';" << endl;
		fileO << "} " << endl;
	}


	// carica KB
	fileO << "function loadKBTree(){" << endl;
	fileO << "	document.getElementById(\"content\").innerHTML='<object type=\"text/html\" data=";
	fileO << "\"Data/KBTree.html\" style=\"width: 100%;height: 100%\"></object>';" << endl;
	fileO << "} " << endl;


	fileO << "		</script>" << endl;

	// Corpo HTML
	fileO << "	</head>" << endl;
	fileO << "	<body>" << endl;


	// Testo
	fileO << "	<div id=\"topBar\"> " << endl;
	fileO << "		<a href=\"#\" onclick=\"loadKBTree()\"  style=\"color: #33cc33\" > Knowledge Base Tree</a>" << endl;
	fileO << "		<br / >" << endl;
	fileO << "		<br / >" << endl;
		// Microprocessi:
	for (int i = 0; i < NMicroProcessi; i++) {
		if (distributionType[i] == 0) {
			fileO << "		<a href=\"#\" onclick=\"loadDistribution" << i << "()\"  style=\"color: #0033cc\" > mp" << i << "</a>" << endl;
		}
		else {
			fileO << "		<a href=\"#\" onclick=\"loadDistribution" << i << "()\"  style=\"color: #ff0000\" > mp" << i << "</a>" << endl;
		}	
	}
	fileO << "		<br / >" << endl;
	fileO << "		<br / >" << endl;
	fileO << "		<a href=\"#\" onclick=\"loadVincoli()\"  style=\"color: #ff00ff\" > Constraints </a>" << endl;
		// Siti:
	fileO << "		<br / >" << endl;
	fileO << "		<br / >" << endl;
	for (int i = 0; i < NSiti; i++) {
		fileO << "		<a href=\"#\" onclick=\"loadSito" << i << "()\"  style=\"color: #0033cc\" > site" << i << "</a>" << endl;
	}

	fileO << "	</div>" << endl;

	fileO << "	<div id=\"content\" style=\"width: 100%; height: 100%; border: 3px solid #73AD21;\"></div>" << endl;

	fileO << "	</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();

	_mkdir(path.append("/Data").c_str());
	// stampo i dati
	this->dumpKBTree(path);
	for (int i = 0; i < NMicroProcessi; i++) {
		this->dumpMicroProcesso(path,i);
	}
	for (int i = 0; i < NSiti; i++) {
		this->dumpSito(path, i);
	}

}

void Instance::dumpMicroProcesso(string path, int nMicroProcesso){
	ostringstream oss;
	oss << path << "/Distribution"<< nMicroProcesso <<".html";
	ofstream fileO(oss.str().c_str());
	

	fileO << "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\">" << endl;
	fileO << "<html>" << endl;
	fileO << "  <head>" << endl;
	fileO << "    <script type=\"text/javascript\ \" src = \" https://www.gstatic.com/charts/loader.js \"></script>" << endl;
	fileO << "    <script type=\"text/javascript\ \">" << endl;
	fileO << "		google.charts.load('current', {packages: ['corechart', 'bar']});" << endl;
	fileO << "		google.charts.setOnLoadCallback(drawBasic);" << endl;

	fileO << "		function drawBasic() {" << endl;

	fileO << "			  var data = new google.visualization.DataTable();" << endl;
	fileO << "			  data.addColumn('number', 'Value');" << endl;
	fileO << "			  data.addColumn('number', 'Probability');" << endl;

	fileO << "			  data.addRows([" << endl;

	for (int i = 0; i < nIstanzeMicroprocessi; i++) {
		fileO << "				["<< i <<", "<< probNInstancePMF[nMicroProcesso][i]<<"]," << endl;
	}
	fileO << "			  ]);" << endl;

	fileO << "			  var options = {" << endl;
	fileO << "				title: 'Distribution'," << endl;
	fileO << "				hAxis: {" << endl;
	fileO << "				  title: 'Value'," << endl;
	fileO << "				}," << endl;
	fileO << "				vAxis: {" << endl;
	fileO << "				  title: 'Probability'" << endl;
	fileO << "				}" << endl;
	fileO << "			  };" << endl;

	fileO << "			  var chart = new google.visualization.ColumnChart(" << endl;
	fileO << "				document.getElementById('chart_div'));" << endl;
	fileO << "			  chart.draw(data, options);" << endl;
	fileO << "		}" << endl;
	fileO << "    </script>" << endl;
	fileO << "  </head>" << endl;
	fileO << "  <body>" << endl;

// TESTO
	fileO << "Microprocess " << nMicroProcesso;
	if (distributionType[nMicroProcesso] == 0) { fileO << " (normal)<br/>" << endl; }
	else { fileO << " (exponential)<br/>" << endl; }

	bool almenoUno = false;
	fileO << " with" << "<br/>" << endl;
	for (int i = 0; i < nCoppieAttivitaSTESSOSito; i++) {
		if (ElencoAttivitaSTESSO[i][0] == nMicroProcesso && ElencoAttivitaSTESSO[i][1] != nMicroProcesso) {
			fileO << ElencoAttivitaSTESSO[i][1] << " || ";
			almenoUno = true;
		}
		else if (ElencoAttivitaSTESSO[i][0] != nMicroProcesso && ElencoAttivitaSTESSO[i][1] == nMicroProcesso) {
			fileO << ElencoAttivitaSTESSO[i][0] << " || ";
			almenoUno = true;
		}
	}
	if (!almenoUno)fileO << "-------" << endl;
	fileO << "<br/>" << endl;
	almenoUno = false;
	fileO << "\t in another site" << "<br/>" << endl;
	for (int i = 0; i < nCoppieAttivitaDIVERSOSito; i++) {
		if (ElencoAttivitaDIVERSO[i][0] == nMicroProcesso && ElencoAttivitaDIVERSO[i][1] != nMicroProcesso) {
			fileO << ElencoAttivitaDIVERSO[i][1] << " || ";
			almenoUno = true;
		}
		else if (ElencoAttivitaDIVERSO[i][0] != nMicroProcesso && ElencoAttivitaDIVERSO[i][1] == nMicroProcesso) {
			fileO << ElencoAttivitaDIVERSO[i][0] << " || ";
			almenoUno = true;
		}
	}
	if (!almenoUno)fileO << "-------" << endl;
	fileO << "<br/>" << endl;
	almenoUno = false;
	fileO << "\t it can be in the following site" << "<br/>" << endl;
	for (int i = 0; i < nMicropocessiInSiti; i++) {
		if (MicropocessiInSiti[i][0] == nMicroProcesso) {
			for (int j = 1; j < MicropocessiInSiti[i].size(); j++) {
				fileO << MicropocessiInSiti[i][j] << " || ";
				almenoUno = true;
			}
		}
	}
	if (!almenoUno)fileO << "-------" << endl;
	fileO << "<br/>" << endl;
	almenoUno = false;
	fileO << "\t we have to measure the following metrics: " << "<br/>" << endl;
	for (int i = 0; i < nMetricheDaMisurare; i++) {
		if (MetricheDaMisurare[i][1] == nMicroProcesso) {
			fileO << MetricheDaMisurare[i][0] << " || ";
			almenoUno = true;
		}
	}
	if (!almenoUno)fileO << "-------" << endl;
	fileO << "<br/>" << endl;
	almenoUno = false;
	fileO << "\t and at least: " << "<br/>" << endl;
	for (int i = 0; i < NDimensioni; i++) {
		if (DimensionRequests[nMicroProcesso][i] == -1) {
			fileO << "Dimension " << i << ": " << "At most 1" << "<br/>" << endl;
			almenoUno = true;
		}else if (DimensionRequests[nMicroProcesso][i] != 0) {
			fileO << "Dimension " << i << ": " << "At least " << DimensionRequests[nMicroProcesso][i] << "<br/>" << endl;
			almenoUno = true;
		}
	}
	if (!almenoUno)fileO << "-------" << endl;
	fileO << "<br/>" << endl;
	fileO << "Media: " << numberInstance[nMicroProcesso] << "<br/>" <<endl;
	fileO << "0.95Quantile: " << quantileMicroprocess(0.95, nMicroProcesso) << "<br/>" << endl;
	almenoUno = false;
	fileO << "    <div id=\"chart_div\"></div>" << endl;
	fileO << "  </body>" << endl;
	fileO << "</html>" << endl;
	
	fileO.close();
}

void Instance::dumpSito(string path, int nSito){
	ostringstream oss;
	oss << path << "/Sito" << nSito << ".html";
	ofstream fileO(oss.str().c_str());

	fileO << "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\">" << endl;
	fileO << "<html>" << endl;
	fileO << "  <head>" << endl;
	fileO << "  </head>" << endl;
	fileO << "  <body>" << endl;

	// TESTO
	fileO << "Sito " << nSito<< "<br/>" << endl;
	fileO << "Costo: "<< CostoUnitarioSito[nSito]<<"<br />";
	fileO << "<br /><br />" << endl;
	fileO << "Costo Valutazione metriche: <br />";
	for (int mm = 0; mm < NMetricMeasurements; mm++)
			fileO << CostoValutazione[mm][nSito] << "\t";

	fileO << "<br /><br />" << endl;
	fileO << "Costo Implementazione metriche: <br />";
	for (int mm = 0; mm < NMetricMeasurements; mm++)
			fileO << CostoSonda[mm][nSito] << "\t";

	fileO << "<br /><br />" << endl;
	fileO << "Precisione metric measurements: <br />";
	for (int mm = 0; mm < NMetricMeasurements; mm++)
		fileO << Precisione[mm][nSito] << " || ";



	fileO << "  </body>" << endl;
	fileO << "</html>" << endl;

	fileO.close();
}

void Instance::initialize(){

	lambdas = new double[NDimensioni];
	Precisione = new double**[NMetricMeasurements];
	Delta1Precisione = new double**[NMetricMeasurements];
	Delta2Precisione = new double**[NMetricMeasurements];
	for (int m = 0; m < NMetricMeasurements; m++) {
		Precisione[m] = new double*[NSiti];
		Delta1Precisione[m] = new double*[NSiti];
		Delta2Precisione[m] = new double*[NSiti];
		for (int s = 0; s < NSiti; s++) {
			Precisione[m][s] = new double[NMicroProcessi];
			Delta1Precisione[m][s] = new double[NMicroProcessi];
			Delta2Precisione[m][s] = new double[NMicroProcessi];
			for (int v = 0; v < NMicroProcessi; v++) {
				double temp = (double)rand() / (RAND_MAX);
				if (temp < probPrecisioneZero) {
					Precisione[m][s][v] = 0.0;
					Delta1Precisione[m][s][v] = 0.0;
				}
				else {
					Precisione[m][s][v] = 0.0;
					Delta1Precisione[m][s][v] = 0.0;
				}
				Delta2Precisione[m][s][v] = 0.0;
			}
		}
	}

	

	ElencoAttivitaSTESSO = new int*[nCoppieAttivitaSTESSOSito];
	for (int i = 0; i < nCoppieAttivitaSTESSOSito; i++) {
		ElencoAttivitaSTESSO[i] = new int[2];
		for (int j = 0; j < 2; j++) {
			ElencoAttivitaSTESSO[i][j] = 0;
			
		}
	}

	ElencoAttivitaDIVERSO = new int*[nCoppieAttivitaDIVERSOSito];
	for (int i = 0; i < nCoppieAttivitaDIVERSOSito; i++) {
		ElencoAttivitaDIVERSO[i] = new int[2];
		for (int j = 0; j < 2; j++) {
			ElencoAttivitaDIVERSO[i][j] = 0;
		}
	}

	MicropocessiInSiti = new vector<int>[nMicropocessiInSiti];
	for (int i = 0; i < nMicropocessiInSiti; i++) {
		MicropocessiInSiti[i].push_back(0);
	}

	// METRICHE DA MISURARE
	MetricheDaMisurare = new int*[nMetricheDaMisurare];
	for (int i = 0; i < nMetricheDaMisurare; i++) {
		MetricheDaMisurare[i] = new int[2];
		MetricheDaMisurare[i][0] = 0;
		MetricheDaMisurare[i][1] = 0;
	}


	// ALMENO N e METRICHE ALMASSIMO 1 METRICHE
	DimensionRequests = new int*[NMicroProcessi];
	for (int v = 0; v < NMicroProcessi; v++) {
		DimensionRequests[v] = new int[NDimensioni];
		for (int i = 0; i < NDimensioni; i++) DimensionRequests[v][i] = 0; // al massimo una
	}

	
	// COSTI
	CostoUnitarioSito = new double[NSiti];

	CostoValutazione = new double*[NMetricMeasurements];
	CostoSonda = new double*[NMetricMeasurements];
	for (int m = 0; m < NMetricMeasurements; m++) {
		CostoValutazione[m] = new double[NSiti];
		CostoSonda[m] = new double[NSiti];
		for (int s = 0; s < NSiti; s++) {
			CostoValutazione[m][s] = 0.0;
			CostoSonda[m][s] = 0.0; 
		}
	}


	MforDimension = new vector<int>[NDimensioni];

	// Generazione MetricMeasurement per ogni metrica
	MMforMetric = new vector<int>[NMetriche];
	
	// numberInstance 
	numberInstance = new int[NMicroProcessi];
	
	// sol 
	sol.x = new int*[NMicroProcessi];
	for (int v = 0; v < NMicroProcessi; v++) {
		sol.x[v] = new int[NSiti];
		for (int s = 0; s < NSiti; s++) {
			sol.x[v][s] = 0;
		}
	}
	sol.y = new int**[NMetricMeasurements];
	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		sol.y[mm] = new int*[NSiti];
		for (int s = 0; s < NSiti; s++) {
			sol.y[mm][s] = new int[NMicroProcessi];
			for (int v = 0; v < NMicroProcessi; v++) {
				sol.y[mm][s][v] = 0;
			}
		}
	}
	sol.z = new int**[NMetricMeasurements];
	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		sol.z[mm] = new int*[NSiti];
		for (int s = 0; s < NSiti; s++) {
			sol.z[mm][s] = new int[NMicroProcessi];
			for (int v = 0; v < NMicroProcessi; v++) {
				sol.z[mm][s][v] = 0;
			}
		}
	}
	sol.w = new int*[NMetriche];
	for (int m = 0; m < NMetriche; m++) {
		sol.w[m] = new int[NMicroProcessi];
		for (int v = 0; v < NMicroProcessi; v++) {
			sol.w[m][v] = 0;
		}
	}
	sol.vv = new int*[NMetriche];
	for (int m = 0; m < NMetriche; m++) {
		sol.vv[m] = new int[NMicroProcessi];
		for (int s = 0; s < NMicroProcessi; s++) {
			sol.vv[m][s] = 0;
		}
	}
	sol.u = new int*[NDimensioni];
	for (int d = 0; d < NDimensioni; d++) {
		sol.u[d] = new int[NMicroProcessi];
		for (int s = 0; s < NMicroProcessi; s++) {
			sol.u[d][s] = 0;
		}
	}
}

void Instance::solveParetoFront(int maxPointsFree, int setPoint,bool saveSol, bool plotPoints, bool verbose) {
	// setPoints = diminuisce il budget di 1/setPoint
	// maxPointFree = finisce l'esecuzione dopo aver trovato maxPointsFree punti

	// provo tutti i valori da budget a zero
	try {
		GRBEnv env = GRBEnv();
		GRBModel model = GRBModel(env);

		// x{vs}: microprocesso v nel sito s
		GRBVar** x = new GRBVar*[NMicroProcessi];
		for (int v = 0; v < NMicroProcessi; v++) {
			x[v] = new GRBVar[NSiti];
			for (int s = 0; s < NSiti; s++) {
				stringstream ss;
				ss << "x" << v << "_" << s;
				string name = ss.str();
				x[v][s] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
			}
		}

		// z_{ms}^v: investo in una stima della metric meaurement m nel sito s quando � presenteil microprocesso v
		GRBVar*** z = new GRBVar**[NMetricMeasurements];
		for (int mm = 0; mm < NMetricMeasurements; mm++) {
			z[mm] = new GRBVar*[NSiti];
			for (int s = 0; s < NSiti; s++) {
				z[mm][s] = new GRBVar[NMicroProcessi];
				for (int v = 0; v < NMicroProcessi; v++) {
					stringstream ss;
					ss << "z_" << mm << "_" << s << "_" << v;
					string name = ss.str();
					z[mm][s][v] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
				}
			}
		}

		

		// y_{ms}^v: investo in una sonda per la metric meaurement m nel sito s quando � presenteil microprocesso v
		GRBVar*** y = new GRBVar**[NMetricMeasurements];
		for (int mm = 0; mm < NMetricMeasurements; mm++) {
			y[mm] = new GRBVar*[NSiti];
			for (int s = 0; s < NSiti; s++) {
				y[mm][s] = new GRBVar[NMicroProcessi];
				for (int v = 0; v < NMicroProcessi; v++) {
					stringstream ss;
					ss << "y_" << mm << "_" << s << "_" << v;
					string name = ss.str();
					y[mm][s][v] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
				}
			}
		}



		// w_{Mv}: stimo una MM della mtrica M per il  microprocesso v
		GRBVar** w = new GRBVar*[NMetriche];
		for (int m = 0; m < NMetriche; m++) {
			w[m] = new GRBVar[NMicroProcessi];
			for (int v = 0; v < NMicroProcessi; v++) {
				stringstream ss;
				ss << "w_" << m << "_" << v;
				string name = ss.str();
				w[m][v] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
			}
		}

		// l: extracost
		GRBVar l = model.addVar(0, DBL_MAX, 0, GRB_CONTINUOUS, "l");


		// Integrate variables into model
		model.update();
		
		GRBLinExpr obj = 0;
		GRBLinExpr expr = 0;

		
		obj = 0;
		for (int d = 0; d < NDimensioni; d++) {
			for (int M = 0; M < MforDimension[d].size(); M++) {
				int idMetrica = MforDimension[d][M];
				for (int mm = 0; mm < MMforMetric[idMetrica].size(); mm++) {
					int idMM = MMforMetric[idMetrica][mm];
					for (int s = 0; s < NSiti; s++)
						for (int v = 0; v < NMicroProcessi; v++) {
							obj += lambdas[d] * (Precisione[idMM][s][v] * x[v][s] + Delta1Precisione[idMM][s][v] * z[idMM][s][v] + Delta2Precisione[idMM][s][v] * y[idMM][s][v]);	
						}
				}	
			}
		}

		obj += -0.1*l/budget;
		
		model.setObjective(obj, GRB_MAXIMIZE); //devo massimizzare e non minimizzare quindi metto il meno


		// ADD CONSTRAINTS
// Ogni microservizio va deploiato
		for (int v = 0; v < NMicroProcessi; v++) {
			expr = 0;
			for (int s = 0; s < NSiti; s++)
				expr += x[v][s];
			model.addConstr(expr, GRB_EQUAL, 1);
		}


// non posso stimare o mettere una sonda se non ho il messo prima il processo
		for (int mm = 0; mm < NMetricMeasurements; mm++)
			for (int s = 0; s < NSiti; s++)
				for (int v = 0; v < NMicroProcessi; v++)
					model.addConstr(y[mm][s][v] + z[mm][s][v], GRB_LESS_EQUAL, x[v][s]);
	
// stimo una MM solo se non c'� nel sito
		for (int mm = 0; mm < NMetricMeasurements; mm++)
			for (int s = 0; s < NSiti; s++)
				for (int v = 0; v < NMicroProcessi; v++)
					model.addConstr(Precisione[mm][s][v], GRB_LESS_EQUAL, 1 - z[mm][s][v]);

// VINCOLI 5,6,7,8,9 E 10
		if (NMicroProcessi >= 10 && NSiti >= 10 && NMetricMeasurements >= 15) {
			
			//  Vincolo su attivit� nello stesso sito
			for (int i = 0; i < nCoppieAttivitaSTESSOSito; i++) {
				for (int s = 0; s < NSiti; s++)
					model.addConstr(x[ElencoAttivitaSTESSO[i][0]][s], GRB_EQUAL, x[ElencoAttivitaSTESSO[i][1]][s]);
			}
			

			//  Vincolo su attivit� in siti diversi siti
			for (int i = 0; i < nCoppieAttivitaDIVERSOSito; i++) {
				for (int s = 0; s < NSiti; s++)
					model.addConstr(x[ElencoAttivitaDIVERSO[i][0]][s] + x[ElencoAttivitaSTESSO[i][1]][s], GRB_LESS_EQUAL, 1);
			}
			
			
			// Vincolo di un sito in un set di siti
			for (int i = 0; i < nMicropocessiInSiti; i++) {
				int indexMP = MicropocessiInSiti[i][0]; // index microprocesso
				expr = 0;
				for (int s = 1; s < MicropocessiInSiti[i].size(); s++) {
					//cout << indexMP << " " << MicropocessiInSiti[i][s] << endl;
					expr += x[indexMP][MicropocessiInSiti[i][s]];
				}
				model.addConstr(expr, GRB_GREATER_EQUAL, 1);
			}

			// Devo misurare alcune metriche per alcuni microporessi.
			for (int i = 0; i < nMetricheDaMisurare; i++) {
				model.addConstr(w[MetricheDaMisurare[i][0]][MetricheDaMisurare[i][1]], GRB_EQUAL, 1);
			}

			// per una particolare dimensione ne devo prendere almeno n
			// DimensionRequests

			for (int v = 0; v < NMicroProcessi; v++) {
				for (int d = 0; d < NDimensioni; d++) {
					if (DimensionRequests[v][d] > 0) {
						expr = 0;
						for (int m = 0; m < MforDimension[d].size(); m++)
							expr += w[m][d];
						model.addConstr(expr, GRB_GREATER_EQUAL, DimensionRequests[v][d]);
						//if(DimensionRequests[v][d] > MforDimension[d].size()) cout<<"MODEL INFEASIBLE"<<endl;
					}

				}
			}

		}
// FINE VINCOLI 5,6,7,8,9 E 10 
/**/


// Precision
		for (int v = 0; v < NMicroProcessi; v++) { // per ogni microservizio
			for (int d = 0; d < NDimensioni; d++) {
				for (int M = 0; M < MforDimension[d].size(); M++) {
					expr = 0;
					int idMetrica = MforDimension[d][M];
					for (int mm = 0; mm < MMforMetric[idMetrica].size(); mm++) {
						int idMM = MMforMetric[idMetrica][mm];
						for (int s = 0; s < NSiti; s++)
							expr += Precisione[idMM][s][v] * x[v][s] + Delta1Precisione[idMM][s][v] * z[idMM][s][v] + Delta2Precisione[idMM][s][v] * y[idMM][s][v];
						// voglio la precisione su ogni mm model.addConstr(minPrecisione, GRB_LESS_EQUAL, expr);
					}
					model.addConstr(minPrecisione*w[idMetrica][v], GRB_LESS_EQUAL, expr);
				}
			}
		}
		
// Costo
		expr = 0;
		for (int s = 0; s < NSiti; s++)
			for (int v = 0; v < NMicroProcessi; v++)
				expr += CostoUnitarioSito[s] * numberInstance[v] * x[v][s];
		for (int mm = 0; mm < NMetricMeasurements; mm++)
			for (int s = 0; s < NSiti; s++)
				for (int v = 0; v < NMicroProcessi; v++)
					expr += CostoValutazione[mm][s] * z[mm][s][v] + CostoSonda[mm][s] * y[mm][s][v];


		model.addConstr(expr, GRB_LESS_EQUAL, l);

		int nParetoPoint = 0;
		while (budget >= 0){
			// costo totale
			model.addConstr(l, GRB_LESS_EQUAL, budget);

			if (!verbose)model.getEnv().set(GRB_IntParam_OutputFlag, 0);
			if (verbose) cout << endl << endl << endl;
			
			clock_t timeStartMethod = clock();
			model.optimize();
			if (saveSol) {

				for (int v = 0; v < NMicroProcessi; v++)
					for (int s = 0; s < NSiti; s++)
						sol.x[v][s] = x[v][s].get(GRB_DoubleAttr_X);
				for (int mm = 0; mm < NMetricMeasurements; mm++)
					for (int s = 0; s < NSiti; s++)
						for (int v = 0; v < NMicroProcessi; v++) {
							sol.z[mm][s][v] = z[mm][s][v].get(GRB_DoubleAttr_X);
							sol.y[mm][s][v] = y[mm][s][v].get(GRB_DoubleAttr_X);
						}
				for (int m = 0; m < NMetriche; m++)
					for (int s = 0; s < NMicroProcessi; s++)
						sol.w[m][s] = w[m][s].get(GRB_DoubleAttr_X);

			}

			hasSolution = true;
			try {
				double* ris = new double[2];
				ris[0] = this->solutionCost();
				objFunction = model.get(GRB_DoubleAttr_ObjVal)+ 0.01*l.get(GRB_DoubleAttr_X) / budget;
				ris[1] = this->solutionAccuracy(); //model.get(GRB_DoubleAttr_ObjVal);
				nParetoPoint++;
				if (plotPoints) {
					//cout.precision(6);
					cout << nParetoPoint << ") " << budget << " [cost:" << ris[0] << ", (E): " << this->nEstimatedMM() << ", (M):" << this->nMadeMM() << "] obj: " << ris[1] << "  t:" << (double)(clock() - timeStartMethod) / CLOCKS_PER_SEC<<endl;
					cout << "[[ ";
					bool* sitiUsati = new bool[NSiti];

					for (int s = 0; s < NSiti; s++) sitiUsati[s] = false;

					for (int s = 0; s < NSiti; s++) 
						for (int v = 0; v < NMicroProcessi; v++) 
							if (x[v][s].get(GRB_DoubleAttr_X) > 0.5) 
								sitiUsati[s] = true;	
					for (int s = 0; s < NSiti; s++) if(sitiUsati[s]) cout << " " << s << "("<< CostoUnitarioSito[s] <<") ";
					cout << " ]]";
					cout << endl << endl;
					//this->dumpSolution("C:/Users/edofa/Desktop");
					
				}
				paretoFront.push_back({ris[0], ris[1]});
				if (setPoint != -1) { budget = ris[0] - ris[0]/setPoint;
				}else{ budget = ris[0] - 0.01; }

				if (nParetoPoint >= maxPointsFree && maxPointsFree != -1) budget = -1;
			}
			catch (...) {
				objFunction = -1;
				if (plotPoints) cout << budget << " infeasible model."<<endl;
				return;
			}
		}


	}
	catch (GRBException e) {
		cout << "Error code = " << e.getErrorCode() << endl;
		cout << e.getMessage() << endl;
		cin.get();
		exit(1);
	}
	catch (...) {
		cout << "Error during optimization" << endl;
		cin.get();
		exit(1);
	}

}

void Instance::setBudget(double budget) {
	this->budget = budget;
}

double Instance::solutionCost(){
	double ris = 0.0;
	for (int s = 0; s < NSiti; s++)
		for (int v = 0; v < NMicroProcessi; v++) {
			if(sol.x[v][s] > 0.5) ris += numberInstance[v]*CostoUnitarioSito[s];
		}
		
	for (int mm = 0; mm < NMetricMeasurements; mm++)
		for (int s = 0; s < NSiti; s++) 
			for (int v = 0; v < NMicroProcessi; v++) {
				if (sol.z[mm][s][v] > 0.5) ris += CostoValutazione[mm][s];
				if (sol.y[mm][s][v] > 0.5) ris += CostoSonda[mm][s];
			}

	return ris;
}

double Instance::solutionAccuracy(){
	double ris = 0.0;

	for (int d = 0; d < NDimensioni; d++) {
		for (int M = 0; M < MforDimension[d].size(); M++) {
			int idMetrica = MforDimension[d][M];
			for (int mm = 0; mm < MMforMetric[idMetrica].size(); mm++) {
				int idMM = MMforMetric[idMetrica][mm];
				for (int s = 0; s < NSiti; s++) {
					for (int v = 0; v < NMicroProcessi; v++) {
						int x_val = 0;
						if (sol.x[v][s] > 0.5) x_val = 1;
						int y_val = 0;
						if (sol.y[idMM][s][v] > 0.5) y_val = 1;
						int z_val = 0;
						if (sol.z[idMM][s][v] > 0.5) z_val = 1;
						ris += (Precisione[idMM][s][v] * x_val + Delta1Precisione[idMM][s][v] * z_val + Delta2Precisione[idMM][s][v] * y_val);
					}
				}
			}
		}
	}


	return ris;
}

int Instance::getY(int mm, int s, int v){
	if (!hasSolution) {
		cout << "Nessuna soluzione, impossibile usare getY(...)" << endl;
		cin.get();
		exit(12312);
	}
	return y_Recorded[mm][s][v];
}

void Instance::plotPareto(string path, int index){
	if (paretoFront.size() == 0) {
		cout << "No Pareto Front computation" << endl;
		return;
	}
	
	ostringstream oss;

	oss << path;
	if (index < 0) {
		oss << "/ParetoFront.csv";
	}else {
		oss << "/ParetoFront"<< index <<".dat";
	}
	ofstream fileO(oss.str().c_str());

	fileO << "l objFunc" << endl;
	for (int i = 0; i < paretoFront.size(); i++) {
		fileO << paretoFront[i].x << " " << paretoFront[i].y << endl;
	}

	
	// MATLAB print:
	// plot(ParetoFront(:,1),ParetoFront(:,2))

}

int Instance::nEstimatedMM() {
	int ris = 0;
	for (int mm = 0; mm < NMetricMeasurements; mm++)
		for (int s = 0; s < NSiti; s++)
			for (int v = 0; v < NMicroProcessi; v++)
				if (sol.z[mm][s][v] > 0.5) ris++;
	return ris;
}

int Instance::nMadeMM() {
	int ris = 0;
	for (int mm = 0; mm < NMetricMeasurements; mm++)
		for (int s = 0; s < NSiti; s++)
			for (int v = 0; v < NMicroProcessi; v++)
				if (sol.y[mm][s][v]> 0.5) ris++;
	return ris;
}


/******************************************/
/******************************************/

void Instance::solveParetoFront_MIN(int maxPointsFree, int setPoint,bool saveSol,double GAP, int timeLimit, bool plotPoints, bool verbose, bool verboseModel) {
	// setPoints = diminuisce il budget di 1/setPoint
	// maxPointFree = finisce l'esecuzione dopo aver trovato maxPointsFree punti
	// provo tutti i valori da budget a zero
	try {
		GRBEnv env = GRBEnv();
		GRBModel model = GRBModel(env);

		// x{vs}: microprocesso v nel sito s
		GRBVar** x = new GRBVar*[NMicroProcessi];
		for (int v = 0; v < NMicroProcessi; v++) {
			x[v] = new GRBVar[NSiti];
			for (int s = 0; s < NSiti; s++) {
				stringstream ss;
				ss << "x" << v << "_" << s;
				string name = ss.str();
				x[v][s] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
			}
		}

		// z_{ms}^v: investo in una stima della metric meaurement m nel sito s quando � presenteil microprocesso v
		GRBVar*** z = new GRBVar**[NMetricMeasurements];
		for (int mm = 0; mm < NMetricMeasurements; mm++) {
			z[mm] = new GRBVar*[NSiti];
			for (int s = 0; s < NSiti; s++) {
				z[mm][s] = new GRBVar[NMicroProcessi];
				for (int v = 0; v < NMicroProcessi; v++) {
					stringstream ss;
					ss << "z_" << mm << "_" << s << "_" << v;
					string name = ss.str();
					z[mm][s][v] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
				}
			}
		}

		// y_{ms}^v: investo in una sonda per la metric meaurement m nel sito s quando � presente il microprocesso v
		GRBVar*** y = new GRBVar**[NMetricMeasurements];
		for (int mm = 0; mm < NMetricMeasurements; mm++) {
			y[mm] = new GRBVar*[NSiti];
			for (int s = 0; s < NSiti; s++) {
				y[mm][s] = new GRBVar[NMicroProcessi];
				for (int v = 0; v < NMicroProcessi; v++) {
					stringstream ss;
					ss << "y_" << mm << "_" << s << "_" << v;
					string name = ss.str();
					y[mm][s][v] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
				}
			}
		}

		// w_{Mv}: stimo una MM della metrica M per il microprocesso v
		GRBVar** w = new GRBVar*[NMetriche];
		for (int m = 0; m < NMetriche; m++) {
			w[m] = new GRBVar[NMicroProcessi];
			for (int v = 0; v < NMicroProcessi; v++) {
				stringstream ss;
				ss << "w_" << m << "_" << v;
				string name = ss.str();
				w[m][v] = model.addVar(0.0, GRB_BINARY, 0.0, GRB_BINARY, name);
			}
		}

		// v_{Ms}: minima precisione per ogni metirca
		GRBVar** vv = new GRBVar*[NMetriche];
		for (int m = 0; m < NMetriche; m++) {
			vv[m] = new GRBVar[NMicroProcessi];
			for (int s = 0; s < NMicroProcessi; s++) {
				stringstream ss;
				ss << "v_" << m << "_" << s;
				string name = ss.str();
				vv[m][s] = model.addVar(0, 1, 0, GRB_CONTINUOUS, name);
			}
		}

		// u_{ds}: minima precisione per ogni dimensione
		GRBVar** u = new GRBVar*[NDimensioni];
		for (int d = 0; d < NDimensioni; d++) {
			u[d] = new GRBVar[NMicroProcessi];
			for (int s = 0; s < NMicroProcessi; s++) {
				stringstream ss;
				ss << "w_" << d << "_" << s;
				string name = ss.str();
				u[d][s] = model.addVar(0, 1, 0, GRB_CONTINUOUS, name);
			}
		}

		// l: extracost
		GRBVar l = model.addVar(0, DBL_MAX, 0, GRB_CONTINUOUS, "l");


		// Integrate variables into model
		model.update();

		GRBLinExpr obj = 0;
		GRBLinExpr expr = 0;

		obj = 0;
		for (int d = 0; d < this->NDimensioni; d++) {
			for (int s = 0; s < this->NMicroProcessi; s++) {
				obj += lambdas[d] * u[d][s];
			}
		}

		obj += -0.1*l / budget;

		model.setObjective(obj, GRB_MAXIMIZE); //devo massimizzare e non minimizzare quindi metto il meno


											   // ADD CONSTRAINTS
											   // Ogni microservizio va deploiato
		for (int v = 0; v < NMicroProcessi; v++) {
			expr = 0;
			for (int s = 0; s < NSiti; s++)
				expr += x[v][s];
			model.addConstr(expr, GRB_EQUAL, 1);
		}


		// non posso stimare o mettere una sonda se non ho il messo prima il processo
		for (int mm = 0; mm < NMetricMeasurements; mm++)
			for (int s = 0; s < NSiti; s++)
				for (int v = 0; v < NMicroProcessi; v++)
					model.addConstr(y[mm][s][v] + z[mm][s][v], GRB_LESS_EQUAL, x[v][s]);

		// stimo una MM solo se non c'� nel sito
		for (int mm = 0; mm < NMetricMeasurements; mm++)
			for (int s = 0; s < NSiti; s++)
				for (int v = 0; v < NMicroProcessi; v++)
					model.addConstr(Precisione[mm][s][v], GRB_LESS_EQUAL, 1 - z[mm][s][v]);

		// VINCOLI 5,6,7,8,9 E 10
		
		//if (NMicroProcessi >= 10 && NSiti >= 10 && NMetricMeasurements >= 15) {
			//  Vincolo su attivit� nello stesso sito
		for (int i = 0; i < nCoppieAttivitaSTESSOSito; i++) {
			for (int s = 0; s < NSiti; s++)
				model.addConstr(x[ElencoAttivitaSTESSO[i][0]][s], GRB_EQUAL, x[ElencoAttivitaSTESSO[i][1]][s]);
		}


		//  Vincolo su attivit� in siti diversi siti
		for (int i = 0; i < nCoppieAttivitaDIVERSOSito; i++) {
			for (int s = 0; s < NSiti; s++)
				model.addConstr(x[ElencoAttivitaDIVERSO[i][0]][s] + x[ElencoAttivitaSTESSO[i][1]][s], GRB_LESS_EQUAL, 1);
		}


		// Vincolo di un sito in un set di siti
		for (int i = 0; i < nMicropocessiInSiti; i++) {
			int indexMP = MicropocessiInSiti[i][0]; // index microprocesso
			expr = 0;
			for (int s = 1; s < MicropocessiInSiti[i].size(); s++) {
				//cout << indexMP << " " << MicropocessiInSiti[i][s] << endl;
				expr += x[indexMP][MicropocessiInSiti[i][s]];
			}
			model.addConstr(expr, GRB_GREATER_EQUAL, 1);
		}

		// Devo misurare alcune metriche per alcuni microporessi.
		for (int i = 0; i < nMetricheDaMisurare; i++) {
			model.addConstr(w[MetricheDaMisurare[i][0]][MetricheDaMisurare[i][1]], GRB_EQUAL, 1);
		}

		// per una particolare dimensione ne devo prendere almeno n
		// DimensionRequests

		for (int v = 0; v < NMicroProcessi; v++) {
			for (int d = 0; d < NDimensioni; d++) {
				if (DimensionRequests[v][d] > 0) {
					expr = 0;
					for (int m = 0; m < MforDimension[d].size(); m++)
						expr += w[m][d];
					model.addConstr(expr, GRB_GREATER_EQUAL, DimensionRequests[v][d]);
					//if(DimensionRequests[v][d] > MforDimension[d].size()) cout<<"MODEL INFEASIBLE"<<endl;
				}

			}
		}
		//}
		// FINE VINCOLI 5,6,7,8,9 E 10 
		/**/


		// Precision
		for (int v = 0; v < NMicroProcessi; v++) { // per ogni microservizio
			for (int M = 0; M < NMetriche ; M++) {
				model.addConstr(minPrecisione*w[M][v], GRB_LESS_EQUAL, vv[M][v]);
			}
		}

		// v definition
		for (int v = 0; v < NMicroProcessi; v++) { // per ogni microservizio
			for (int d = 0; d < NDimensioni; d++) {
				for (int M = 0; M < MforDimension[d].size(); M++) {
					int idMetrica = MforDimension[d][M];
					for (int mm = 0; mm < MMforMetric[idMetrica].size(); mm++) {
						int idMM = MMforMetric[idMetrica][mm];
						expr = 0;
						for (int s = 0; s < NSiti; s++)
							expr += Precisione[idMM][s][v] * x[v][s] + Delta1Precisione[idMM][s][v] * z[idMM][s][v] + Delta2Precisione[idMM][s][v] * y[idMM][s][v];
						model.addConstr(vv[M][v], GRB_LESS_EQUAL, expr);
					}
				}
			}
		}


		// u definition
		for (int v = 0; v < NMicroProcessi; v++) { // per ogni microservizio
			for (int d = 0; d < NDimensioni; d++) {
				expr=0;
				for (int M = 0; M < MforDimension[d].size(); M++) {
					expr+=vv[M][v];
					//model.addConstr(u[d][v], GRB_LESS_EQUAL, vv[M][v]);
				}
				model.addConstr(u[d][v], GRB_LESS_EQUAL, expr);
			}
		}

		// Costo
		expr = 0;
		for (int s = 0; s < NSiti; s++)
			for (int v = 0; v < NMicroProcessi; v++)
				expr += CostoUnitarioSito[s] * numberInstance[v] * x[v][s];
		for (int mm = 0; mm < NMetricMeasurements; mm++)
			for (int s = 0; s < NSiti; s++)
				for (int v = 0; v < NMicroProcessi; v++)
					expr += CostoValutazione[mm][s] * z[mm][s][v] + CostoSonda[mm][s] * y[mm][s][v];
		model.addConstr(expr, GRB_LESS_EQUAL, l);

		if (!verboseModel) model.getEnv().set(GRB_IntParam_OutputFlag, 0);

		if (verbose) cout << endl << endl << endl;

		// metto un gap del 10%

		int nParetoPoint = 0;
		double* ris = new double[2];
		
		int sumY = 0;
		int sumZ = 0;
		bool cont = true;

		while (budget >= 0 && cont) {
			
			// costo totale
			model.addConstr(l, GRB_LESS_EQUAL, budget);

			// Aggiungere OF se ho gi� un punto
			if (nParetoPoint >= 1) {
				cout << "aggiungo  vincoli (budget: "<<ris[1] <<" - sumY: "<< sumY - 1 << " - sumZ: " << sumZ + 1 <<")"<< endl;
				//	- # delle y scelte <= #yscelte al passo prima-1
				expr = 0;
				for (int mm = 0; mm < NMetricMeasurements; mm++)
					for (int s = 0; s < NSiti; s++)
						for (int v = 0; v < NMicroProcessi; v++)
							expr += y[mm][s][v];
				model.addConstr(expr, GRB_LESS_EQUAL, sumY-1);
				//	- # delle z scelte >= #zscelte al passo prima-1
				expr = 0;
				for (int mm = 0; mm < NMetricMeasurements; mm++)
					for (int s = 0; s < NSiti; s++)
						for (int v = 0; v < NMicroProcessi; v++)
							expr += z[mm][s][v];
				model.addConstr(expr, GRB_GREATER_EQUAL, sumZ + 1);
			}

			// oppure se ho letto la soluzione ( non funziona)
			if (sumY_OLD >= 0) {
				for (int mm = 0; mm < NMetricMeasurements; mm++)
					for (int s = 0; s < NSiti; s++)
						for (int v = 0; v < NMicroProcessi; v++)
							expr += y[mm][s][v];
				model.addConstr(expr, GRB_LESS_EQUAL, sumY_OLD);

				//	- # delle z scelte >= #zscelte al passo prima-1
				expr = 0;
				for (int mm = 0; mm < NMetricMeasurements; mm++)
					for (int s = 0; s < NSiti; s++)
						for (int v = 0; v < NMicroProcessi; v++)
							expr += z[mm][s][v];
				model.addConstr(expr, GRB_GREATER_EQUAL, sumZ_OLD);

				expr = 0;
				for (int d = 0; d < this->NDimensioni; d++) {
					for (int s = 0; s < this->NMicroProcessi; s++) {
						expr += lambdas[d] * u[d][s];
					}
				}
				model.addConstr(expr, GRB_LESS_EQUAL, accuracy_OLD);

			}
			if (timeLimit > -1) model.getEnv().set(GRB_DoubleParam_TimeLimit, timeLimit);
			if (GAP > -1) model.getEnv().set(GRB_DoubleParam_MIPGap, GAP);

			try {
				clock_t tStart = clock();
				model.optimize();
				double timeOpt = (double)(clock() - tStart) / CLOCKS_PER_SEC;
				hasSolution = true;

				sumY = 0;
				sumZ = 0;
				for (int mm = 0; mm < NMetricMeasurements; mm++)
					for (int s = 0; s < NSiti; s++)
						for (int v = 0; v < NMicroProcessi; v++) {
							sumY += y[mm][s][v].get(GRB_DoubleAttr_X);
							sumZ += z[mm][s][v].get(GRB_DoubleAttr_X);
						}

				if (saveSol){
					for (int v = 0; v < NMicroProcessi; v++)
						for (int s = 0; s < NSiti; s++)
							sol.x[v][s] = x[v][s].get(GRB_DoubleAttr_X);
					for (int mm = 0; mm < NMetricMeasurements; mm++)
						for (int s = 0; s < NSiti; s++)
							for (int v = 0; v < NMicroProcessi; v++) {
								sol.z[mm][s][v] = z[mm][s][v].get(GRB_DoubleAttr_X);
								sol.y[mm][s][v] = y[mm][s][v].get(GRB_DoubleAttr_X);
							}
					for (int m = 0; m < NMetriche; m++)
						for (int s = 0; s < NMicroProcessi; s++) {
							sol.w[m][s] = w[m][s].get(GRB_DoubleAttr_X);
							sol.vv[m][s] = vv[m][s].get(GRB_DoubleAttr_X);
						}
					for (int d = 0; d < NDimensioni; d++)
						for (int s = 0; s < NMicroProcessi; s++)
							sol.u[d][s] = u[d][s].get(GRB_DoubleAttr_X);

					sol.l=l.get(GRB_DoubleAttr_X);

				}

			
				ris[0] = this->solutionCost();
				objFunction = model.get(GRB_DoubleAttr_ObjVal) + 0.01*l.get(GRB_DoubleAttr_X) / budget;
				ris[1] = this->solutionAccuracy();
				cout <<"---->"<< ris[0] << "  " << ris[1] << endl;
				nParetoPoint++;
				if (plotPoints) {
					cout << nParetoPoint << ") " << budget << " [cost:" << ris[0] << ", (E): " << this->nEstimatedMM() << ", (M):" << this->nMadeMM() << "] obj: " << ris[1] << endl;
					cout << "[[ ";
					bool* sitiUsati = new bool[NSiti];

					for (int s = 0; s < NSiti; s++) sitiUsati[s] = false;

					for (int s = 0; s < NSiti; s++)
						for (int v = 0; v < NMicroProcessi; v++)
							if (x[v][s].get(GRB_DoubleAttr_X) > 0.5)
								sitiUsati[s] = true;

					for (int s = 0; s < NSiti; s++) if (sitiUsati[s]) cout << " " << s << "(" << CostoUnitarioSito[s] << ") ";
					cout << " ]]"<<endl;
					cout<<"Gap: " << model.get(GRB_DoubleAttr_MIPGap) << endl;
					cout << "Time: " << timeOpt  << endl;
					cout << endl << endl;
					//this->dumpSolution("C:/Users/edofa/Desktop");
				}
				paretoFront.push_back({ ris[0], ris[1] });
				if (setPoint != -1) {
					budget = ris[0] - ris[0] / setPoint;
				}
				else { budget = ris[0] - 0.01; }

				if (nParetoPoint >= maxPointsFree && maxPointsFree != -1) cont = false;
			}

			catch (...) {
				objFunction = -1;
				if (plotPoints) cout << budget << "infeasible model."<<endl;
				return;
			}
		}
	}
	catch (GRBException e) {
		cout << "Error code = " << e.getErrorCode() << endl;
		cout << e.getMessage() << endl;
		if (plotPoints) cout << budget << "infeasible model." << endl;
		return;
		//cin.get();
		//exit(1);
	}
	catch (...) {
		cout << "Error during optimization" << endl;
		if (plotPoints) cout << budget << "infeasible model." << endl;
		return;
		//cin.get();
		//exit(1);
	}

}

void Instance::dumpSolution_MIN(string path) {
	if (!hasSolution) {
		cout << "Nessuna soluzione da stampare" << endl;
		return;
	}
	vector<int> SitiUsati;
	for (int s = 0; s < NSiti; s++) {
		for (int v = 0; v < NMicroProcessi; v++) {
			if (sol.x[v][s] > 0.5) {
				SitiUsati.push_back(s);
				break;
			}
		}
	}

	ofstream fileO(string(path).append("/solution.html").c_str());
	fileO << "<html>" << endl;
	fileO << "	<head>" << endl;
	fileO << "		<script type=\"text/javascript\">" << endl;

	
	for (int i = 0; i < SitiUsati.size(); i++){
		fileO << "function loadSito" << SitiUsati[i] << "(){" << endl;
		fileO << "	document.getElementById(\"content\").innerHTML='<object type=\"text/html\" data=";
		fileO << "\"Data/SitoSol"<< SitiUsati[i] <<".html\" style=\"width: 100%;height: 100%\"></object>';" << endl;
		fileO << "} " << endl;
	}
	
	fileO << "		</script>" << endl;
	fileO << "	</head>" << endl;


	fileO << "<body>" << endl;
	fileO << "Accuracy: " << this->solutionAccuracy() << "  ||  Cost: " << this->solutionCost() << "<br/>" << "<br/>" << endl;

	// Testo
	fileO << "	<div id=\"topBar\"> " << endl;
	for (int i = 0; i < SitiUsati.size(); i++) {
		fileO << "<a href=\"#\" onclick=\"loadSito" << SitiUsati[i] << "()\"  style=\"color: #ff00ff\" > loadSito" << SitiUsati[i] << " </a>" << endl;
	}
	fileO << "	</div>" << endl;


	fileO << "	<div id=\"content\" style=\"width: 100%; height: 100%; border: 3px solid #73AD21;\"></div>" << endl;

	fileO << "Costo soluzione: " << "<br/>" << endl;
	fileO << "Std (moltiplicato per il numero di istanze): ";
	double costoStd = 0;
	for (int s = 0; s < NSiti; s++) {
		double parziale = 0.0;
		for (int v = 0; v < NMicroProcessi; v++) {
			if (sol.x[v][s] > 0.5) {
				costoStd += numberInstance[v] * CostoUnitarioSito[s];
				parziale += numberInstance[v] * CostoUnitarioSito[s];
			}
		}
		fileO << parziale << "(" << s << ") ";
	}

	fileO << " = " << costoStd << "<br/>" << endl;
	fileO << endl << "Stime: ";
	double costoEst = 0;
	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		for (int s = 0; s < NSiti; s++) {
			for (int v = 0; v < NMicroProcessi; v++) {
				if (sol.z[mm][s][v] > 0.5) {
					costoEst += CostoValutazione[mm][s];
				}
			}
		}
	}
	fileO << " = " << costoEst << endl;
	fileO << "<br/>" << endl << "Sonde: ";

	double costoProb = 0;
	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		for (int s = 0; s < NSiti; s++) {
			for (int v = 0; v < NMicroProcessi; v++) {
				if (sol.y[mm][s][v] > 0.5) {
					costoProb += CostoSonda[mm][s];
					//fileO << CostoSonda[mm][s] << "(" << mm << "--" << s << ") ";
				}
			}
		}
	}
	fileO << " = " << costoProb << "<br/>" << endl;
	fileO << "<br/>" << endl << "Costo Totale: " << costoStd + costoEst + costoProb << endl;// << " quindi l e' uguale a : " << l.get(GRB_DoubleAttr_X) << endl;
	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();


	_mkdir(path.append("/Data").c_str());
	for (int i = 0; i < SitiUsati.size(); i++) {
		this->printSiteSolution(SitiUsati[i], path);
	}

	

}

void Instance::dumpSolutionEssential_MIN(string path, int index){
	
	if (!hasSolution) {
		cout << "Nessuna soluzione da stampare" << endl;
		return;
	}

	ostringstream oss;
	oss << path;
	if (index < 0) {
		oss << "/solution.txt";
	}
	else {
		oss << "/solution" << index << ".txt";
	}
	ofstream fileO(oss.str().c_str());
	
	for (int s = 0; s < NSiti; s++){
		for (int v = 0; v < NMicroProcessi; v++) {
			if (sol.x[v][s] > 0.5) fileO << v <<" " << s << endl;
		}
	}
	
	fileO << "---" << endl;

	for (int mm = 0; mm < NMetricMeasurements; mm++)
		for (int s = 0; s < NSiti; s++)
			for (int v = 0; v < NMicroProcessi; v++) {
				if (sol.z[mm][s][v] > 0.5) fileO<<v<<" "<< s<<" "<<mm <<endl;
			}
	
	fileO << "---" << endl;

	for (int mm = 0; mm < NMetricMeasurements; mm++)
		for (int s = 0; s < NSiti; s++)
			for (int v = 0; v < NMicroProcessi; v++) {
				if (sol.y[mm][s][v] > 0.5) fileO << v << " "<< s << " " << mm << endl;
			}

	fileO << "---" << endl;
	
	fileO << this->solutionCost() << endl;
	fileO << this->solutionAccuracy() << endl;

	fileO.close();
}

void Instance::readSolutionEssential_MIN(string path, bool verbose){
	string line;
	string word;

	ifstream iffN(path.c_str());

	if (!iffN.is_open()) {
		cout << "Solution not found in " << path << endl;
		return;
	}


	// scarto tutte le x
	std::getline(iffN, line);
	while (line.compare("---") != 0) {
		std::getline(iffN, line);
	}

	// leggo le z
	std::getline(iffN, line);
	sumZ_OLD = 0;
	while (line.compare("---") != 0) {
		std::getline(iffN, line);
		if (verbose) cout << line << endl;
		sumZ_OLD++;
	}
	if(verbose) cout<<"OLD sumZ: " << sumZ_OLD << endl;
	// leggo le y
	std::getline(iffN, line);
	sumY_OLD = 0;
	while (line.compare("---") != 0) {
		std::getline(iffN, line);
		sumY_OLD++;
	}

	if (verbose) cout <<"OLD sumY: " << sumY_OLD << endl;
	std::getline(iffN, line);
	//cost_OLD = atof(line.c_str());
	//if (verbose) cout<<"OLD Costs: " << cost_OLD << endl;
	std::getline(iffN, line);
	accuracy_OLD = atof(line.c_str());
	if (verbose) cout<<"OLD Accuracy: " << accuracy_OLD << endl;

	iffN.close();
}

void Instance::printSiteSolution(int nSito, string path){
	ostringstream oss;
	oss << path << "/SitoSol" << nSito << ".html";
	ofstream fileO(oss.str().c_str());
	fileO << "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\">" << endl;
	fileO << "<html>" << endl;
	fileO << "	<head>" << endl;
	fileO << "	</head>" << endl;
	fileO << "<body>" << endl;
	int NStimate = 0;
	int NSonde = 0;
	fileO << "<div>" << endl;
	fileO << "SITO " << nSito << "<br/>" << endl;
	fileO << "\t Microprocessi':" << "<br/>" << endl;

	for (int v = 0; v < NMicroProcessi; v++) {
		if (sol.x[v][nSito] > 0.5) {
			fileO << "\t" << v << "||  ";
		}
	}
	fileO << "<br/>" << endl;
	fileO << "<div style=\" float:left; width:50%;\">" << endl;
	fileO << "\t metriche stimate:" << "<br/>" << endl;

	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		bool flag = false;
		bool* who = new bool[NMicroProcessi];
		for (int v = 0; v < NMicroProcessi; v++)
			if (sol.z[mm][nSito][v] > 0.5) {
				flag = true;
				who[v] = true;
			}
			else {
				who[v] = false;
			}
			if (flag) {
				fileO << "\t" << mm << endl;
				fileO << "(  ";
				for (int v = 0; v < NMicroProcessi; v++)
					if (who[v])
						fileO << v << " ";
				fileO << "  )";
				fileO << "<br/>" << endl;
				NStimate++;
			}
	}
	if (NStimate == 0) {
		fileO << "\t ----";
	}
	fileO << " tot Stimate: " << NStimate << " <br/></div>" << endl;
	fileO << "<div style=\" float:right; width:50%;\">" << endl;
	fileO << "\t sonde implementate:" << "<br/>" << endl;

	for (int mm = 0; mm < NMetricMeasurements; mm++) {
		bool flag = false;
		bool* who = new bool[NMicroProcessi];
		for (int v = 0; v < NMicroProcessi; v++)
			if (sol.y[mm][nSito][v] > 0.5) {
				flag = true;
				who[v] = true;
			}
			else {
				who[v] = false;
			}
			if (flag) {
				fileO << "\t" << mm << endl;
				fileO << "(  ";
				for (int v = 0; v < NMicroProcessi; v++)
					if (who[v])
						fileO << v << " ";
				fileO << "  )";
				fileO << "<br/>" << endl;
				NSonde++;
			}
	}

	if (NSonde == 0) {
		fileO << "\t ----";
	}

	fileO << " tot Probes: " << NSonde << "<br/>"<<endl;
	fileO << "</div>" << endl;

	fileO << "</div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;

	fileO.close();
}