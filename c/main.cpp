#include <sstream>
#include <string>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <cmath>
#include <deque>
#include <iomanip>

#include "gurobi_c++.h"
#include "instance.h"


using namespace std;


int main(int argc,char *argv[]){
	/*
	int NDimensioni = 10;
	int NMetriche = 30;
	int NMetricMeasurements = 50;
	int NAttivita = 100;
	int NSiti = 10;
	string path = "C:/Users/CloudUser/Desktop";
	double probPrecisioneZero = 0.5;
	srand(time(NULL)); // use it only once, e.g. at the start of main()
	/**/
	// -i cartella input -o cartella output

	string pathInput;
	string pathOutput;
	string input = "-i";
	string output = "-o";
	/**/
	for (int count = 1; count < argc; ++count) {
		if (output.compare(argv[count])==0)
			pathOutput = argv[count + 1];

		if (input.compare(argv[count]) == 0)
			pathInput = argv[count + 1];
	}
	//cout << pathInput << " " << pathOutput << endl;
		
	//pathInput = "C:/Users/edofa/Desktop";
	//pathOutput = "C:/Users/edofa/Desktop";
	//pathInput = "C:/Users/Edoardo/Desktop";
	//pathOutput = "C:/Users/Edoardo/Desktop";
	
	string pathOutput1 = pathOutput;
	Instance* in = new Instance(pathInput.append("/Instance.txt"));
	in->solveParetoFront_MIN(1, 100, true, -1, 300);
	in->dumpSolutionEssential_MIN(pathOutput, -1);

	/*
	Instance* in = new Instance(NDimensioni, NMetriche, NMetricMeasurements, NAttivita, NSiti, probPrecisioneZero);
	in->generate(false);
	//in->printInstance(pathInput);
	//in->solveParetoFront_MIN(1, 1, true, 0.1, 60);
	//in->dumpSolutionEssential_MIN("C:/Users/edofa/Desktop");


	/*
	double gap = -1;
	int time = 60;
	
	Instance* in = new Instance(NDimensioni, NMetriche, NMetricMeasurements, NAttivita, NSiti, probPrecisioneZero);
	in->generate(false);
	in->printInstance(pathInput, 0);
	in->dumpInstance(pathInput);
	in->solveParetoFront_MIN(1, 100, true, gap, time);
	in->dumpSolutionEssential_MIN(pathOutput,0);
	in->printInstance(pathInput, 1);
	cout << "Accuracy: " << in->solutionAccuracy() << " Costo Totale: " << in->solutionCost() << endl;
	/*
	//Instance* in1 = new Instance("C:/Users/Edoardo/Desktop/Instance1.txt");
	
	Instance* in1 = new Instance("C:/Users/Edoardo/Desktop/instance_generated.txt");
	
	//Instance* in1 = new Instance("C:/Users/Edoardo/Desktop/Instance1.txt");
	in1->solveParetoFront_MIN(1, 100, true, gap, time);
	in1->dumpSolutionEssential_MIN(pathInput, 1);
	in1->printInstance(pathOutput, 2);

	cout << "Accuracy: " << in1->solutionAccuracy() << " Costo Totale: " << in1->solutionCost() << endl;
	/*
	Instance* in2 = new Instance("C:/Users/edofa/Desktop/Instance2.txt");
	in2->solveParetoFront_MIN(1, 100, true, gap, time);
	in2->dumpSolutionEssential_MIN("C:/Users/edofa/Desktop", 2);
	in2->printInstance("C:/Users/edofa/Desktop", 3);

	cout << "Accuracy: " << in2->solutionAccuracy() << " Costo Totale: " << in2->solutionCost() << endl;

	/*Leggere soluzione
	// readSolutionEssential_MIN non funziona
	double gap = -1;
	int time = 60;
	Instance* in = new Instance(NDimensioni, NMetriche, NMetricMeasurements, NAttivita, NSiti, probPrecisioneZero);
	in->generate(false);
	in->solveParetoFront_MIN(1, 100, true, gap, time);
	in->dumpSolutionEssential_MIN("C:/Users/edofa/Desktop", 0);
	in->printInstance("C:/Users/edofa/Desktop", 1);
	
	cout << "=================" << endl;

	Instance* in1 = new Instance("C:/Users/edofa/Desktop/Instance1.txt");
	in1->readSolutionEssential_MIN("C:/Users/edofa/Desktop/solution0.txt");
	in1->solveParetoFront_MIN(1, 100, true, gap, time);


	/**/
	/*
	int REPETITIONS = 1;
	Instance* in = new Instance(NDimensioni, NMetriche, NMetricMeasurements, NAttivita, NSiti, probPrecisioneZero);
	//Instance* in = new Instance(4, 4, 4, 15, 6, probPrecisioneZero);
	in->generate(false);
	//in->printInstance("C:/Users/Edoardo/Desktop",0);
	in->printInstance("C:/Users/edofa/Desktop", 0);
	Instance* in1 = new Instance("C:/Users/edofa/Desktop/Instance0.txt");
	//in1->readInstance("C:/Users/Edoardo/Desktop/Instance0.txt");
	//in1->printInstance("C:/Users/Edoardo/Desktop",1);
	in1->printInstance("C:/Users/edofa/Desktop", 1);
	in1->solveParetoFront_MIN(1, 1, true, 0.1, 60);
	/**/

	/* SOLUZIONE NORMALE
	for (int i = 1; i <= 10; i++) {
		Instance* in = new Instance(NDimensioni, NMetriche, NMetricMeasurements, NAttivita, NSiti, probPrecisioneZero);
		in->generate(false);
		in->solveParetoFront_MIN(10, 100, true, 0.1, 60);
		in->plotPareto("C:/Users/Edoardo/Desktop", i);
	}
	//in->dumpSolution_MIN("C:/Users/edofa/Desktop");
	//in->dumpSolution_MIN("C:/Users/Edoardo/Desktop");
	/**/



	/*DISTRIBUTION
	Instance* in = new Instance(NDimensioni, NMetriche, NMetricMeasurements, NAttivita, NSiti, probPrecisioneZero);
	in->generate(false);
	in->dumpInstance("C:/Users/Edoardo/Desktop");
	//in->solveTest(1000, "C:/Users/edofa/Desktop", 1);
	in->solveTest(10000, "C:/Users/edofa/Dropbox/BackUp Lavori/Plebani/NuoviRisultati/Scenari", 1);
	//in->solveTest(1000, "D:/Dropbox/BackUp Lavori/Plebani/NuoviRisultati/Scenari", 1);
	
	for (int i = 1; i <= 10; i++) {
		cout << i << endl;
		in->modifyMeanwithQuantile(i/10);
		//in->dumpInstance("C:/Users/edofa/Desktop");
		//in->solveTest(1000, "C:/Users/edofa/Desktop", i+1);
		//in->solveTest(5000, "D:/Dropbox/BackUp Lavori/Plebani/NuoviRisultati/Scenari", i + 1);
		in->solveTest(10000, "C:/Users/edofa/Dropbox/BackUp Lavori/Plebani/NuoviRisultati/Scenari", i+1);
	}
	
	/*FRONTIERE DI PARETO
	//int i = 0;
	for (int i = 0; i < 10; i++){
		Instance* in = new Instance(NDimensioni, NMetriche, NMetricMeasurements, NAttivita, NSiti, probPrecisioneZero);
		//cout << "Instance Generation..." << endl;
		in->generate(false);
		//in->dumpInstance("C:/Users/Edoardo/Desktop");
		//in->dumpInstance("C:/Users/edofa/Desktop");
		//cout << "Instance Generated..." << endl << endl;
		cout << "mm: "<< NMetricMeasurements<<" s: "<<NSiti<<" VM: "<<NAttivita << endl << endl;
		//in->solve();
		//in->solveParetoFront(1);
		in->solveParetoFront_MIN(-1,10,0.2,-1,true,false,true);
		//in->plotPareto("C:/Users/edofa/Desktop", i);
		in->plotPareto("C:/Users/Edoardo/Desktop",i);
		//in->plotPareto("C:/Users/CloudUser/Desktop", i);
	}
	/**/

	/*
	int NDimensioni = 10;
	int NMetriche = 30;
	int NMetricMeasurements = 50;
	int NAttivita = 100;
	int NSiti = 10;
	string path = "";
	double probPrecisioneZero = 0.5;
	srand(time(NULL));
	path = "C:/Users/edofa/Desktop";
	//path = "C:/Users/Edoardo/Desktop";
	/*ESPERIMENTI CRESCITA KNOWLEDGEBASE
	ofstream fileKB(string(path).append("/MetricMeasurementIncrease_MIN.csv").c_str());
	for (int i = 1; i <= 16; i++) {
		fileKB << NMetricMeasurements*i << "; ";
		double time = 0.0;
		for (int rep = 0; rep < REPETITIONS; rep++) {
			Instance* in = new Instance(NDimensioni*i / 3, NMetriche*i / 2, NMetricMeasurements*i, NAttivita, NSiti, probPrecisioneZero);
			//cout << "Instance Generation..." << endl;
			in->generate(false);
			//in->dumpInstance("C:/Users/Edoardo/Desktop");
			//cout << "Instance Generated..." << endl << endl;
			clock_t timeStartMethod = clock();
			in->solveParetoFront(1);
			time += (double)(clock() - timeStartMethod) / CLOCKS_PER_SEC;
			cout << "-----------------" << endl;
			cout << "MM " << i << ") " << NMetricMeasurements*i << "; " << time / (REPETITIONS + 0.0) << endl;
			cout << "-----------------" << endl;
			fileKB << time  << "; ";
		}
		fileKB << endl;
	}
	fileKB.close();

	/*ESPERIMENTI CRESCITA SITI
	ofstream fileCP(string(path).append("/SitesIncrease_MIN.csv").c_str());
	for (int i = 1; i <= 20; i++) {
		fileCP << NSiti*i << "; ";
		double time = 0.0;
		for (int rep = 0; rep < REPETITIONS; rep++) {
			Instance* in = new Instance(NDimensioni, NMetriche, NMetricMeasurements, NAttivita, NSiti*i, probPrecisioneZero);
			in->generate(false);
			//in->dumpInstance("C:/Users/Edoardo/Desktop");
			clock_t timeStartMethod = clock();
			in->solveParetoFront(1);
			time += (double)(clock() - timeStartMethod) / CLOCKS_PER_SEC;
			cout << "-----------------" << endl;
			cout <<"Siti " <<i << ") " << NSiti*i << "; " << time / (REPETITIONS + 0.0) << endl;
			cout << "-----------------" << endl;
			fileCP << time << "; ";
		}
		fileCP << endl;
	}
	fileCP.close();

	/*ESPERIMENTI CRESCITA MICROSERVICE
	ofstream fileMS(string(path).append("/MicroservicesIncrease_MIN.csv").c_str());
	for (int i = 1; i <= 40; i+=5) {
		fileMS << NAttivita*i << "; ";
		double time = 0.0;
		for (int rep = 0; rep < REPETITIONS; rep++) {
			Instance* in = new Instance(NDimensioni, NMetriche, NMetricMeasurements, NAttivita*i, NSiti, probPrecisioneZero);
			in->generate(false);
			//in->dumpInstance("C:/Users/Edoardo/Desktop");
			clock_t timeStartMethod = clock();
			in->solveParetoFront(1);
			time += (double)(clock() - timeStartMethod) / CLOCKS_PER_SEC;
			cout << "-----------------" << endl;
			cout << "Attivita' " << i << ") " << NAttivita*i << "; " << time / (REPETITIONS + 0.0) << endl;
			cout << "-----------------" << endl;
			fileMS  << time  << "; ";
		}
		fileMS << endl;
	}
	fileMS.close();

	/*ESPERIMENTO EXTREME
	NDimensioni = 10;
	NMetriche = 100;
	NMetricMeasurements = 300;
	NAttivita = 100;
	NSiti = 100;	
	ofstream fileO(string(path).append("/Extreme.csv").c_str());
	for (int i = 1; i <= 10; i++) {
		Instance* in = new Instance(NDimensioni, NMetriche, NMetricMeasurements, NAttivita, NSiti, probPrecisioneZero);
		cout << i << " ";
		//cout << "Instance Generation..." << endl;
		in->generate(false);
		//cout << "Instance Generated..." << endl << endl;
		clock_t timeStartMethod = clock();
		in->solveParetoFront(1);
		fileO << (double)(clock() - timeStartMethod) / CLOCKS_PER_SEC << endl;
		cout << (double)(clock() - timeStartMethod) / CLOCKS_PER_SEC << endl;
	}
	fileO.close();
	/**/
	cin.get();
	return 0;
}