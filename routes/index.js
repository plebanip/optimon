var express = require('express');
var router = express.Router();
var fs = require('fs');

var init_session = require('../init_session.js');

/* GET home page. */
router.get('/', function (req, res, next) {


    init_session.init(req);

    var session_info = req.session;

    var filename_requests = session_info.requestsFilename;
    var filename_offerings =  session_info.offeringsFilename;
    var filename_kb = session_info.KBFilename;

    try {
        var requests = require(filename_requests);

        var offerings = require(filename_offerings);
        var kb = require(filename_kb);

        res.render('index', {requestsText: JSON.stringify(requests, undefined, 2), offeringsText: JSON.stringify(offerings, undefined, 2), kbText: JSON.stringify(kb, undefined, 2)});

    } catch (err) {
        console.log(err);

        res.render('error', {error: err.message});
    }



});



module.exports = router;
