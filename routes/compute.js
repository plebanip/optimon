var express = require('express');
var uuid = require('node-uuid');
var router = express.Router();
var fs = require('fs');
//var exec = require('child_process').exec;
var spawn = require('child_process').spawn;

var EventLogger = require('node-windows').EventLogger;
var sudo = require('node-windows').sudo;
var elevate = require('node-windows').elevate;
var log = new EventLogger('Optimon');

var init_session = require('../init_session.js');

//var convertRequests = require('../bin/convertRequests.js');
//var convertOfferings = require('../bin/convertOfferings.js');
//var convertkb = require('../bin/convertKB.js');
var convertGurobi = require('../bin/createGurobiFile.js');
var gurobiCaller = require('../bin/callGurobi.js')
var convertResponse = require('../bin/createResponse.js')

router.post('/', function (req, res, next) {


  init_session.init(req);

  var session_info = req.session;

  //var filename_requests = "uploads/" + session_info.requestsFilename;
  //var filename_offerings = "uploads/" + session_info.offeringsFilename;
  //var filename_kb = "uploads/" + session_info.KBFilename;

  var path = __dirname + '\\..\\';
  var filename_requests = '..\\temp\\' + uuid.v4() + '-rq.json';
  var filename_offerings = '..\\temp\\' + uuid.v4() + '-of.json';
  var filename_kb = '..\\temp\\' + uuid.v4() + '-kb.json';

  session_info.requestsFilename = filename_requests;
  session_info.offeringsFilename = filename_offerings;
  session_info.KBFilename = filename_kb;

  filename_requests = __dirname + '\\' + filename_requests;
  filename_offerings = __dirname + '\\' + filename_offerings;
  filename_kb = __dirname + '\\' + filename_kb;

  //console.log(req.body);

  var filename_input = path + 'gurobi-instances\\' + uuid.v4();
  fs.writeFileSync(filename_requests, req.body.requestsFileTextArea);
  fs.writeFileSync(filename_offerings, req.body.offeringsFileTextArea);
  fs.writeFileSync(filename_kb, req.body.kbFileTextArea);
  //var cmd = path + 'bin\\CloudComputing.exe -i ' + filename_input.replace('/', '\\') + '.txt -o ' + filename_input.replace('/', '\\') + '-out.txt'
  var cmd = path + 'bin\\CloudComputing.exe';
  const args = ['-i', filename_input.replace('/', '\\') + '.txt',
                '-o', filename_input.replace('/', '\\') + '-out.txt'];

  var options = {
      encoding: 'utf8',
      timeout: 0,
      maxBuffer: 1000*1024,
      killSignal: 'SIGTERM',
      cwd: null,
      env: null
  }
  console.log('sto chiamando ' + cmd);
  console.log(args);

  //rememebr!!! both node and CloudComputing must run as admininstrator to be included in the win service
  convertGurobi.createGurobiFile(filename_input, filename_offerings, filename_requests, filename_kb, function() {
    var child = spawn(cmd, args);
    child.stdout.on('data', function(data) {
      //log.error('stdout');
      console.log(data.toString());
      var data = fs.readFileSync(filename_input+'-out.txt', 'utf8');
      var result = convertResponse.createResponse(filename_input+'-out.txt', filename_kb);
      res.render('gurobi', {input: result});
    });
    child.stderr.on('data', function(data) {
      //log.error('stdout');
      console.log(data.toString());
      res.render('error', {message:'Error while executing Gurobi', error: data.toString()});
    });
    child.on('exit', function(code) {
      //log.error('stdout');
      console.log(code.toString());
    });
    /*var child = sudo(cmd, '', function(error, stdout, stderr) {
      log.error(cmd);
      if (error) {
        res.render('error', {message:error.code, error: error.signal});
      }
      if (stdout) {
        var data = fs.readFileSync(filename_input+'-out.txt', 'utf8');
        var result = convertResponse.createResponse(filename_input+'-out.txt', filename_kb);
        res.render('gurobi', {input: result});
      }
      if (stderr) {
        res.render('error', {message:'errore', error: 'errore'});
      }
      res.render('error', {message:'eseguito', error: 'cs'});
    });*/

  })





});

module.exports = router;
