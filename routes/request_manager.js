var express = require('express');
var router = express.Router();
var multer = require('multer');

var upload = multer({dest: 'uploads/'})

var init_session = require('../init_session.js');

router.post('/', upload.single('requestsFile'), function (req, res, next) {

    init_session.init(req);
        
    req.session.requestsFilename = req.file.filename;
        
    res.redirect('/');

});


module.exports = router;
