var express = require('express');
var router = express.Router();
var fs = require('fs');
var multer = require('multer');

var upload = multer({dest: 'uploads/'})

var init_session = require('../init_session.js');


router.post('/', upload.single('offeringsFile'), function (req, res, next) {
    //res.render('index', { title: 'Express' });

     init_session.init(req);
     console.log(req.file);
        
    req.session.offeringsFilename = req.file.filename;
    res.redirect('/');

});


module.exports = router;
