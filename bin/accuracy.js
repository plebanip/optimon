

exports.convert = function (offeringsFilename, requestsFilename, kbFilename, func) {


  var offerings = require(offeringsFilename).cloud_provider_offer;
  var requests = require(requestsFilename).application_owner_request;
  var kb = require(kbFilename);



  var std_str = "Std Prec\n\n";
  var sti_str = "Stim Prec\n\n";
  var imp_str = "Stim Meas\n\n";



  for (ms=0; ms < requests.microservices.length; ms++) {
    var found_ms = 0;
    var metrics_requested = [];

    if (requests.requests != null) {

      for (r = 0; r < requests.requests.length; r++) {
        if (requests.requests[r].microservice === requests.microservices[ms]) {
          found_ms = 1;
          for (kd = 0; kd < kb.dimensions.length; kd++) {

            var found_dimension = 0;
            if (requests.requests[r].qom_constraints != null && requests.requests[r].qom_constraints.constraints != null) {
              for (c = 0; c < requests.requests[r].qom_constraints.constraints.length; c++) {

                if (kb.dimensions[kd].dimension === requests.requests[r].qom_constraints.constraints[c].dimension) {
                  found_dimension = 1;
                  if (requests.requests[r].qom_constraints.constraints[c].trans_policy === "ALL") {
                    for (km = 0; km < kb.dimensions[kd].metrics.length; km++) {
                      metrics_requested.push({"metric": kb.dimensions[kd].metrics[km].metric, "sampling": requests.requests[r].qom_constraints.constraints[c].sampling, "prec": requests.requests[r].qom_constraints.constraints[c].prec});
                    }
                  }
                 if (requests.requests[r].qom_constraints.constraints[c].trans_policy === "XOR") {
                    for (km = 0; km < kb.dimensions[kd].metrics.length; km++) {
                      metrics_requested.push({"metric": kb.dimensions[kd].metrics[km].metric, "sampling": requests.requests[r].qom_constraints.constraints[c].sampling, "prec": requests.requests[r].qom_constraints.constraints[c].prec});
                    }
                  }
                  if (requests.requests[r].qom_constraints.constraints[c].trans_policy === "none") {
                    for (km = 0; km < kb.dimensions[kd].metrics.length; km++) {
                      var found_metric = 0;
                      for (rm = 0; rm < requests.requests[r].qom_constraints.constraints[c].metrics.length; rm++) {
                        if (kb.dimensions[kd].metrics[km].metric === requests.requests[r].qom_constraints.constraints[c].metrics[rm].metric) {
                          found_metric = 1;
                          metrics_requested.push({"metric": kb.dimensions[kd].metrics[km].metric, "sampling": requests.requests[r].qom_constraints.constraints[c].metrics[rm].sampling, "prec": requests.requests[r].qom_constraints.constraints[c].metrics[rm].prec});
                        }
                      }
                      if (found_metric === 0) {
                        metrics_requested.push({"metric": kb.dimensions[kd].metrics[km].metric, "sampling": -1, "prec": requests.requests[r].qom_constraints.constraints[c].prec});
                      }
                    }
                  }
                }
              }
            }
            if (found_dimension === 0) {
              for (km = 0; km < kb.dimensions[kd].metrics.length; km++) {
                metrics_requested.push({"metric": kb.dimensions[kd].metrics[km].metric, "sampling": -1});
              }
            }
          }
        }
      }
    }
    if (found_ms ===0) {
      for (kd = 0; kd < kb.dimensions.length; kd++) {
        for (km = 0; km < kb.dimensions[kd].metrics.length; km++) {
          metrics_requested.push({"metric": kb.dimensions[kd].metrics[km].metric, "sampling": -1});
        }
      }
    }

    for (i = 0; i < metrics_requested.length; i++) {

      for (kd = 0; kd < kb.dimensions.length; kd++) {
        for (km = 0; km < kb.dimensions[kd].metrics.length; km++) {
          if (metrics_requested[i].metric === kb.dimensions[kd].metrics[km].metric) {
            for (kmm=0; kmm < kb.dimensions[kd].metrics[km].measurements.length; kmm++) {
              for (o = 0; o < offerings.budget_profile.length; o++) {
                var offer_sampling = offerings.budget_profile[o].sampling;
                if (metrics_requested[i].prec == "detailed") {
                  if (metrics_requested[i].sampling / offer_sampling > 1) {
                    std_str = std_str + "1" + "\t";
                    imp_str = imp_str + "0" + "\t";
                  } else {
                    std_str = std_str + (metrics_requested[i].sampling / offer_sampling) + "\t";
                    imp_str = imp_str + (1-((metrics_requested[i].sampling / offer_sampling))) + "\t";
                  }
                  sti_str = sti_str + "0" + "\t";

                } else if (metrics_requested[i].prec == "trend") {
                  std_str = std_str + "0" + "\t";
                  sti_str = sti_str + "1" + "\t";
                  imp_str = imp_str + "1" + "\t";
                } else if (metrics_requested[i].prec == "none") {
                  std_str = std_str + "0" + "\t";
                  sti_str = sti_str + "1" + "\t";
                  imp_str = imp_str + "1" + "\t";
                } else {
                  std_str = std_str + "1" + "\t";
                  sti_str = sti_str + "0" + "\t";
                  imp_str = imp_str + "0" + "\t";
                }

              }
              //add space for new metric
              std_str = std_str + "\n";
              sti_str = sti_str + "\n";
              imp_str = imp_str + "\n";
            }
          }
        }
      }
    }

    //add space for new matrix next service
    std_str = std_str + "\n";
    sti_str = sti_str + "\n";
    imp_str = imp_str + "\n";

  }

  func(std_str + "\n" + sti_str + "\n" + imp_str + "\n");

}
