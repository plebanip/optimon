

exports.convert = function (requestsFilename, kbFilename, func) {


    var requests = require(requestsFilename).application_owner_request.requests;
    var kb = require(kbFilename);

    var mtm_str = "Metric to meas\n";
    var aln_str = "";
    var count = 0;
    if (requests != null) {

    for (i = 0; i < requests.length; i++) {

      if (requests[i].qom_constraints != null && requests[i].qom_constraints.constraints != null) {
        for (j = 0; j < requests[i].qom_constraints.constraints.length; j++) {
            if (requests[i].qom_constraints.constraints[j].trans_policy === "none") {

                for (k = 0; k < requests[i].qom_constraints.constraints[j].metrics.length; k++) {
                    mtm_str = mtm_str + requests[i].qom_constraints.constraints[j].metrics[k].metric.replace(/[^0-9]/g,'') + "\t" + requests[i].microservice.replace(/[^0-9]/g,'') + "\n";
                }
                aln_str = aln_str + requests[i].microservice.replace(/[^0-9]/g,'') + "\t" + requests[i].qom_constraints.constraints[j].dimension.replace(/[^0-9]/g,'') + "\t" + requests[i].qom_constraints.constraints[j].metrics.length + "\n";
                count ++;
            } else if (requests[i].qom_constraints.constraints[j].trans_policy === "XOR") {
                aln_str = aln_str + requests[i].microservice.replace(/[^0-9]/g,'') + "\t" + requests[i].qom_constraints.constraints[j].dimension.replace(/[^0-9]/g,'') + "\t" + "-1" + "\n";
                count++;

            } else if (requests[i].qom_constraints.constraints[j].trans_policy === "ALL") {

                for (r = 0; r < kb.dimensions.length; r++) {
                    if (kb.dimensions[r].dimension === requests[i].qom_constraints.constraints[j].dimension) {
                        aln_str = aln_str + requests[i].microservice.replace(/[^0-9]/g,'') + "\t" + requests[i].qom_constraints.constraints[j].dimension.replace(/[^0-9]/g,'') + "\t" + kb.dimensions[r].metrics.length + "\n";
                        count++;
                    }
                }

            }
        }
      }
    }
    }


    aln_str = "At least N\n" + count + "\n" + aln_str;

    func(mtm_str + "\n" + aln_str + "\n");


}
