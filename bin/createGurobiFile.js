
var general = require('./general.js');
var accuracy = require('./accuracy.js');
var constraints = require('./constraints.js');
var metrics = require('./metrics.js');
var costs = require('./costs.js');
var kb = require('./kb.js');

var fs = require('fs')

var out_str = "";

exports.createGurobiFile = function(fileInput, offeringsFilename, requestsFilename, kbFilename, func) {

  general.convert(requestsFilename, offeringsFilename, kbFilename, function (data) {
    out_str = data + "\n";

    accuracy.convert(offeringsFilename, requestsFilename, kbFilename, function (data) {

      out_str = out_str + data;

      constraints.convert(requestsFilename, function (data) {

        out_str = out_str + data;

        metrics.convert(requestsFilename, kbFilename, function (data) {
          out_str = out_str + data;

          costs.convert(requestsFilename, offeringsFilename, kbFilename, function (data) {
            out_str = out_str + data;

            kb.convert(kbFilename, function (data) {
              out_str = out_str + data;
              out_str = out_str + "\nlambda\n";
              out_str = out_str + "1\t1\t1\n\n"
              out_str = out_str + "nIstanze\n";
              out_str = out_str + "1\t1\t1\t1\n"

              fs.writeFile(fileInput + '.txt', out_str);
              console.log('geenrated ' + fileInput + '.txt');
              func();
            });
          });
        });

      });


    });

  });
}
