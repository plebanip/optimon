

exports.convert = function (requestsFilename, offeringsFilename, kbFilename, func) {


    var kb = require(kbFilename);
    var requests = require(requestsFilename).application_owner_request;
    var offerings = require(offeringsFilename).cloud_provider_offer;

//    var fs= require('fs');
//    var kb = JSON.parte(fs.readFileSync(kbFilename, 'utf-8'));
//    var requests = JSON.parte(fs.readFileSync(requestsFilename, 'utf-8'));

    console.log(requests);

    var m = 0;
    var mm = 0;

    var out_str = "General Coeff\n";

    for (i = 0; i < kb.dimensions.length; i++) {
        m = m + kb.dimensions[i].metrics.length;
        for (j = 0; j < kb.dimensions[i].metrics.length; j++) {
            mm = mm + kb.dimensions[i].metrics[j].measurements.length;
        }
    }

    var same = 0;
    var diff = 0;
    var att = 0;
    var mtm = 0;

    var ms = requests.microservices.length;
    var off = offerings.offerings;

    out_str = out_str + kb.dimensions.length + "\t" + m + "\t" + mm + "\t" + ms + "\t" + off +"\n";

    if (requests.dep_constraints != null) {
      for (i = 0;   i < requests.dep_constraints.length; i++) {
        if (requests.dep_constraints[i].type === "same") {
            for (j = 0; j < requests.dep_constraints[i].ms_list.length; j++) {
                for (k = j + 1; k < requests.dep_constraints[i].ms_list.length; k++) {
                    same++;
                }
            }
        } else if (requests.dep_constraints[i].type === "diff") {
            for (j = 0; j < requests.dep_constraints[i].ms_list.length; j++) {
                for (k = j + 1; k < requests.dep_constraints[i].ms_list.length; k++) {
                    diff++;
                }
            }
        } else if (requests.dep_constraints[i].type === "pref") {
            for (j = 0; j < requests.dep_constraints[i].ms_list.length; j++) {
                att++;
            }
        }
      }
    }

    if (requests.requests != null) {
      for (i = 0; i < requests.requests.length; i++) {
        if (requests.requests[i].qom_constraints != null && requests.requests[i].qom_constraints.constraints != null) {
          for (j = 0; j < requests.requests[i].qom_constraints.constraints.length; j++) {
            if (requests.requests[i].qom_constraints.constraints[j].trans_policy === "none") {

                for (k = 0; k < requests.requests[i].qom_constraints.constraints[j].metrics.length; k++) {
                    mtm++;
                }

            }
          }
        }
      }
    }




    out_str = out_str + same + "\t" + diff + "\t" + att + "\t" + mtm + "\n";

    out_str = out_str + requests.budget.maxbudget + "\t" + "0.1" + "\n";

    func(out_str);


}
