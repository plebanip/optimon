

exports.convert = function(requestsFilename, func) {


    var requests = require(requestsFilename).application_owner_request;

    var out_str_same = "Constr Same Site\n";
    var out_str_diff = "Constr Different Site\n";
    var out_str_insite = "Constr Microproc in sites\n";

    if (requests.dep_constraints != null) {

      for (i=0; i<requests.dep_constraints.length; i++ ) {
        if (requests.dep_constraints[i].type === "same") {
            for (j=0; j<requests.dep_constraints[i].ms_list.length; j++) {
                for (k=j+1; k<requests.dep_constraints[i].ms_list.length; k++) {
                    out_str_same = out_str_same + requests.dep_constraints[i].ms_list[j].replace(/[^0-9]/g,'') + "\t" + requests.dep_constraints[i].ms_list[k].replace(/[^0-9]/g,'') + "\n";
                }
            }
        } else if (requests.dep_constraints[i].type === "diff") {
            for (j=0; j<requests.dep_constraints[i].ms_list.length; j++) {
                for (k=j+1; k<requests.dep_constraints[i].ms_list.length; k++) {
                    out_str_diff = out_str_diff + requests.dep_constraints[i].ms_list[j].replace(/[^0-9]/g,'') + "\t" + requests.dep_constraints[i].ms_list[k].replace(/[^0-9]/g,'') + "\n";
                }
            }
        } else if (requests.dep_constraints[i].type === "pref") {
            for (j=0; j<requests.dep_constraints[i].ms_list.length; j++) {
                out_str_insite = out_str_insite + requests.dep_constraints[i].ms_list[j].replace(/[^0-9]/g,'') + "\t" + requests.dep_constraints[i].providers.length + "\t";
                for (k=0; k<requests.dep_constraints[i].providers.length; k++) {
                    out_str_insite = out_str_insite + requests.dep_constraints[i].providers[k].replace(/[^0-9]/g,'') + "\t";
                }
                out_str_insite = out_str_insite + "\n";
            }
        }
      }
    }

    func(out_str_same + "\n" + out_str_diff + "\n" + out_str_insite+"\n");
}




/*
* Constr Same Site
6	7	 						// attività 6 e 7 devono stare nello stesso sito
8	7
6   5

Constr Different Site
4	1							// attività 4 e 1 devono stare in siti diversi
5   2
4   3

Constr Microproc in sites
8	1	1						// microprocesso, # di siti in cui può stare, elenco siti e.g. il processo 8 può stare in 1 sito (l'8)
7	2	4	5					// processo 7 può stare in 2 siti, il 4 e il 5
 */
