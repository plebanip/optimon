var fs = require('fs');


exports.createResponse = function(outFilename, kbFilename) {

  var kb = require(kbFilename);
//  var requests = require(requestsFilename).application_owner_request;
//  var offerings = require(offeringsFilename).cloud_provider_offer;
  var data = fs.readFileSync(outFilename);


  var res = data.toString().split('\n');

  var section = 0;
  var pairs = [];
  var estimation = [];
  var implementation = [];
  var cost = {"cost": -1};
  var accuracy = {"accuracy": -1};

  for (i = 0; i< res.length; i++) {
      if (res[i].trim() === "---") {
        section++;
      } else {
        if (section === 0) {
          var p = res[i].split(" ");
          pairs.push({"service": "ms" + p[0], "provider": "cp" + p[1].trim()});
        } else if (section === 1 || section ===2) {
          var p = res[i].split(" ");
          var mm_name = "";
          for (kd =0; kd < kb.dimensions.length; kd++) {
            for (km = 0; km < kb.dimensions[kd].metrics.length; km++) {
              for (kmm = 0; kmm < kb.dimensions[kd].metrics[km].measurements.length; kmm++) {
                if (kb.dimensions[kd].metrics[km].measurements[kmm].measurement === ("mm"+p[2].trim())) {
                  mm_name = kb.dimensions[kd].metrics[km].measurements[kmm].name;
                }
              }
            }
          }
          if (section === 1) {
            estimation.push({"service": "ms" + p[0], "provider": "cp" + p[1], "measurements": mm_name});
          } else {
            implementation.push({"service": "ms" + p[0], "provider": "cp" + p[1], "measurements": mm_name});
          }
        } else if (section === 3) {
            if (cost.cost === -1) {
              cost.cost = res[i].trim();
            } else if (accuracy.accuracy === -1){
              accuracy.accuracy = res[i].trim();
            }
        }
      }
  }

  result = {"assignment": pairs, "estimated_mm": estimation, "implementated_mm": implementation, "cost": cost.cost, "accuracy": accuracy.accuracy};
  return result;

}


//createResponse('../gurobi-instances/4606e5fe-c3b7-4645-b412-85d1a520933a-out.txt', '../uploads/requests.json', '../uploads/offerings.json', '../uploads/kb.json');
