

exports.convert = function(requestsFilename, offeringsFilename, kbFilename, func) {


  var requests = require(requestsFilename);
  var offerings = require(offeringsFilename).cloud_provider_offer;
  var kb = require(kbFilename)

  var std_costs_str = "Std Costs\n";
  var eval_costs_str = "Eval Costs\n";
  var impl_costs_str = "Impl Costs\n";

  var no_sites = offerings.budget_profile.length;

  for (i=0; i<no_sites; i++) {
    std_costs_str = std_costs_str + offerings.budget_profile[i].cost_per_inst + "\t";
  }
  std_costs_str = std_costs_str + "\n";

  var cost_metrics = [];
  for (kd = 0; kd < kb.dimensions.length; kd++ ) {
    for (km = 0; km < kb.dimensions[kd].metrics.length; km++) {
      var km_found_est = 0;
      var km_found_cus = 0;
      for (i=0; i<offerings.est_metrics.metrics.length; i++) {
        if (offerings.est_metrics.metrics[i].metric === kb.dimensions[kd].metrics[km].metric) {
          km_found_est = 1;
        }
      }
      for (i=0; i<offerings.custom_metrics.metrics.length; i++) {
        if (offerings.custom_metrics.metrics[i].metric === kb.dimensions[kd].metrics[km].metric) {
          km_found_cus = 1;
        }
      }
      for (kmm = 0; kmm < kb.dimensions[kd].metrics[km].measurements.length; kmm++) {
        for (j=0; j<no_sites; j++) {
          if (km_found_est == 1) {
            eval_costs_str = eval_costs_str + offerings.est_metrics.cost + "\t";
          } else {
            eval_costs_str = eval_costs_str + "1000" + "\t";
          }
          if (km_found_cus == 1) {
            impl_costs_str = impl_costs_str + offerings.custom_metrics.cost + "\t";
          } else {
            impl_costs_str = impl_costs_str + "1000" + "\t";
          }

        }
        eval_costs_str = eval_costs_str + "\n";
        impl_costs_str = impl_costs_str + "\n";
      }
    }

  }

  // for (i=0; i<offerings.est_metrics.metrics.length; i++) {
  //     for (j=0; j<no_sites; j++) {
  //         eval_costs_str = eval_costs_str + offerings.est_metrics.cost + "\t";
  //     }
  //     eval_costs_str = eval_costs_str + "\n";
  // }
  //
  // for (i=0; i<offerings.custom_metrics.metrics.length; i++) {
  //     for (j=0; j<no_sites; j++) {
  //         impl_costs_str = impl_costs_str + offerings.custom_metrics.cost + "\t";
  //     }
  //     impl_costs_str = impl_costs_str + "\n";
  // }

  func(std_costs_str + "\n" + eval_costs_str + "\n" + impl_costs_str+ "\n");


}
