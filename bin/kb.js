

exports.convert = function (kbFilename, func) {

    var kb = require(kbFilename);

    var ametrics = [];
    var completemetrics = [];

    var kb_str = "Know Tree\n";
    for (i = 0; i < kb.dimensions.length; i++) {
        kb_str = kb_str + kb.dimensions[i].dimension.replace(/[^0-9]/g,'') + "\t" + kb.dimensions[i].metrics.length;
        for (j = 0; j < kb.dimensions[i].metrics.length; j++) {
            kb_str = kb_str + "\t" + kb.dimensions[i].metrics[j].metric.replace(/[^0-9]/g,'');

            if (ametrics.indexOf(kb.dimensions[i].metrics[j].metric) === -1) {
                ametrics.push(kb.dimensions[i].metrics[j].metric);
                completemetrics.push(kb.dimensions[i].metrics[j]);
            }

        }
        kb_str = kb_str + "\n";
    }

    kb_str = kb_str + "\n";

    for (i = 0; i < completemetrics.length; i++) {

        kb_str = kb_str + completemetrics[i].metric.replace(/[^0-9]/g,'') + "\t" + completemetrics[i].measurements.length;

        for (j = 0; j < completemetrics[i].measurements.length; j++) {
            kb_str = kb_str + "\t" + completemetrics[i].measurements[j].measurement.replace(/[^0-9]/g,'');

        }

        kb_str = kb_str + "\n";

    }


    func(kb_str);


}

//convert("uploads/kb.json", function (data) {

//    console.log(data);
//});


/*Know Tree
 0	 1	 0	// dimensione, # di metriche, elenco metriche
 1	 2	 1	3 // dimensione 1 ha 2 metriche legate, la 1 e la 3
 2	 1	 2

 0	 1	 0	// metrica, # di mm, elenco mm
 1	 1	 1
 2	 3	 2 4 5 // metrica 2 ha 3 metriche legate, la 2 e la 4 e la 5
 */
